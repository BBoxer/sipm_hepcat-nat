#!/usr/bin/env python3.9
# File name: create_integTime_skimFiles.py
# Description: loops through a set of given integration times and creates a skimmed file containing the area assoc. with that time, and the filenames and event IDs.
# All events in all runParams_v2 processed files are looped over.
# Author: Jyothisraj Johnson (jyjohnson@ucdavis.edu)
# Date: 05-10-22

import sys
sys.path.append("../..")
import logging
from logging import critical, error, info, warning, debug
import argparse
from asic_utils import asic_utils as au
from asic_utils import plot_utils as pu

import time
import numpy as np
import pandas as pd
from pathlib import Path
import math
import multiprocessing as mp
import glob

def parse_arguments():
    """Read arguments from a command line."""

    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')
    parser.add_argument('-d', '--dir_path', type=str, help='directory where data directories are held',
                        default='/group/tripathilab/projects/neutron_camera/data/')
    parser.add_argument('-f', '--data_folder', type=str, help='folder containing data files',
                        default= 'runParams_v2/')
    parser.add_argument('-o', '--out_path', type=str, help='output directory to save dictionaries as .pki files',
                        default='Stilbene')
    parser.add_argument('-s', '--sample_time', type=int, help='what is the sample period of the digitizer? i.e. for 250MS/s, sample_time = 4',
                        default='4')
    parser.add_argument('-m_t1', '--max_t1', type=int, help='max t1 integration time to create skim file for',
                        default='200') 
    parser.add_argument('-m_t2', '--max_t2', type=int, help='max t2 integration time to create skim file for',                                                                                         
                        default='2000')
    parser.add_argument('-s_t1', '--step_t1', type=int, help='t1 integration number of steps',
                        default='5')
    parser.add_argument('-s_t2', '--step_t2', type=int, help='t1 integration number of steps',
                        default='5')

    args = parser.parse_args()
    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)

    return args

def get_areas_mp(filenames, t_int, pre_peak_sample, sample_time=4, num_files=30, verbose=1):
    #t_int should be a single integration value
    
    df_key = 'FOUT peak' #this function assumes we are interested in using FOUT for T0 selection
    tmp_areas = []
    tmp_filenames = []
    tmp_evtIDs = []

    error_ct = 0
    for count, file in enumerate(filenames):
        if verbose >= 1:
            print("Current file ({}/{}) open is: {}".format(count+1,len(filenames),file), flush = True)
        #load in tmp pandas dataframe for given directory, folder, and file
        tmp_runParamv2_df = pd.read_pickle(file)
        for evt in range(len(tmp_runParamv2_df[df_key])):
            peak_sample_tmp = int(tmp_runParamv2_df[df_key][evt]) #get the relevant FOUT peak sample for an evt
            T0_tmp = peak_sample_tmp - pre_peak_sample #define T0 for grabbing the relevant cummulative integration value
            if verbose >= 3:
                print(T0_tmp, flush = True)
            t_int_sample = int(t_int / sample_time) #convert time in ns to sample
            if verbose >= 3:
                print("t_int_sample = {}".format(t_int_sample), flush = True)
            
            tmp_idx = T0_tmp + t_int_sample
            try:
                tmp_areas.append(tmp_runParamv2_df['SOUT area_mA_ns'][evt][tmp_idx] - tmp_runParamv2_df['SOUT area_mA_ns'][evt][T0_tmp])
            except:
                error_ct += 1
                if verbose >= 2:
                    print("Total errors is now: {}. Current error from short areas.".format(error_ct), flush = True)
                tmp_areas.append(-1)
            tmp_filenames.append(tmp_runParamv2_df['file_index'][evt])
            tmp_evtIDs.append(tmp_runParamv2_df['event'][evt])
            if evt % 100000 == 0:
                if verbose >= 1:
                    print("Current event number being processed is: {}".format(evt), flush = True)
    
    return tmp_areas, tmp_evtIDs, tmp_filenames

def save_areas_mp(areas, filenames, evtIDs, file_to_save, out_filepath):
    dict_to_save = {}
    dict_to_save['areas'] = areas
    dict_to_save['filenames'] = filenames
    dict_to_save['evtIDs'] = evtIDs
    df_to_save = pd.DataFrame.from_dict(dict_to_save)
    df_to_save.to_pickle(out_filepath+file_to_save)
    print('Finished saving file: {}'.format(file_to_save), flush = True)

  
def main(args):
    #Set presample window prior to FOUT peak
    pre_peak_sample_FOUT = 1

    #Load in the files based on parser choices
    filenames = (glob.glob(args.dir_path +args.data_folder+'*'))
    
    #Check files were found
    if len(filenames) < 1:
        print('No files found. Exiting', flush = True)
        exit()
    else:
        print("{} file(s) found".format(len(filenames)), flush = True)
   
    #Generate step sizes from max_t1/t2 and num_steps_t1/t2
    step_t1 = int((args.max_t1 / args.step_t1))
    step_t2 = int((args.max_t2 / args.step_t2))

    t_int = []
    for t1 in range(step_t1, args.max_t1 + step_t1, step_t1):
        t_int.append(t1)
    for t2 in range(step_t2, args.max_t2 + step_t2, step_t2):
        t_int.append(t2)
    
    print('There are {} t_int values to create skim files for.'.format(len(t_int)), flush = True)

    for tmp_t_int in t_int:
        print('Current tmp_t_int is: {}'.format(tmp_t_int), flush = True)
        tmp_trig = 'foutTriggering'
        file_base = filenames[0].split('/')[-1][:-17]
        file_to_save = file_base + '_{}_tInt_{}_sampleRate_{}_corrected'.format(tmp_trig, tmp_t_int, args.sample_time)
        tmp_path = Path(args.out_path+file_to_save)
        if not (tmp_path.is_file()):       
            tmp_areas, tmp_evtIDs, tmp_filenames = get_areas_mp(filenames, tmp_t_int, pre_peak_sample_FOUT)
            save_areas_mp(areas=tmp_areas, filenames=tmp_filenames, evtIDs=tmp_evtIDs, file_to_save=file_to_save, out_filepath=args.out_path)
            del tmp_areas
            del tmp_evtIDs
            del tmp_filenames
        else:
            print('Output File: {} already exits. Moving to Next File...'.format(file_to_save))

if __name__ == '__main__':
    #Timer for benchmarking
    start_time = time.time()

    #Get the command line arguements from parse
    args = parse_arguments()
    print("\nStarting create_integTime_skimFiles.py\nUsing:\n{}".format(args), flush = True)
   
    #Execute main
    main(args)

    print('\nRun time: {}\nEnd\n\n'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time))), flush = True)
