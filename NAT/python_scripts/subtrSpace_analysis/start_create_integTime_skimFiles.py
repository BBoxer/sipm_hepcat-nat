#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 23:02:14 2021

@author: jyothisjohnson1
"""

import sys
import os

###########################################################

''' 
Change variables below as needed. 
'''

DIR_PATH = '/group/tripathilab/projects/neutron_camera/data/runParams_v2/'

'''Co57 folders below'''
# DATA_FOLDER = 'Stilbene_6mm3_CH19FB_Co57_Data_3e7samples/' 
# DATA_FOLDER = 'EJ276_6mm3_CH19FB_Co57_Data_3e7samples/' 
DATA_FOLDER = 'EJ276_50mm3_CH19FB_Co57_Data_3e7samples/' 

'''Cf252 folders below'''
# DATA_FOLDER = 'Stilbene_6mm3_CH19FB_Cf252_Data_3e7samples/' 
# DATA_FOLDER = 'EJ276_6mm3_CH19FB_Cf252_Data_3e7samples/' 
# DATA_FOLDER = 'EJ276_50mm3_CH19FB_Cf252_Data_3e7samples/' 

'''AmBe folders below'''
# DATA_FOLDER = 'Stilbene_6mm3_CH19FB_AmBe_Data_3e7samples/' 
# DATA_FOLDER = 'EJ276_6mm3_CH19FB_AmBe_Data_3e7samples/' 
# DATA_FOLDER = 'EJ276_50mm3_CH19FB_AmBe_Data_3e7samples/' 

OUT_PATH = '/home/jyjohn24/projects/neutron_camera/saved_areaFiles_v2/'
SAMPLE_TIME = 4
MAX_T1 = 500
MAX_T2 = 2000
NUM_STEP_T1 = 10
NUM_STEP_T2 = 10

##########################################################

''' 
Put together command based on above selected variable values.
'''
args = 'python create_integTime_skimFiles.py '
args += '-d ' + DIR_PATH + ' '
args += '-f ' + DATA_FOLDER + ' '
args += '-o ' + OUT_PATH + ' '
args += '-s ' + str(SAMPLE_TIME) + ' '
args += '-m_t1 ' + str(MAX_T1) + ' '
args += '-m_t2 ' + str(MAX_T2) + ' '
args += '-s_t1 ' + str(NUM_STEP_T1) + ' '
args += '-s_t2 ' + str(NUM_STEP_T2) + ' '

'''
Finally, issue command.
'''
os.system(args)
