#!/usr/bin/env python3.9.5
# File name: check_xml_to_pickle.py
# Description: Checks the .pki trace files to see if they have been converted from the xml successfully. 
# Deletes any files that do not load correctly. Files that are successfully are stored in a txt file
# Author: Billy Boxer (bboxer@ucdavis.edu)
# Date: 04-08-22

import sys
from xmlrpc.client import boolean
import pandas as pd
import pathlib
from pathlib import Path
import numpy as np
import glob
import logging
from logging import critical, error, info, warning, debug
import argparse
import gc

sys.path.append("../..") # Adds higher directory to python modules path.
from asic_utils import asic_utils as au
from asic_utils import plot_utils as pu

def parse_arguments():
    """Read arguments from a command line."""

    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')
    parser.add_argument('-d', '--directory', type=str, help='directory where data directories are held',
                        default='/group/tripathilab/projects/neutron_camera/data_DT5751/')
    parser.add_argument('-b', '--bias', type=int, help='select a single bias to run over, default runs over all',
                        default='29')
    parser.add_argument('-t', '--threshold', type=str, help='select a single threhsold to run over, default runs over all',
                        default='0037')
    parser.add_argument('-sc', '--scintillator', type=str, help='select scintillator',
                        default='Stilbene')
    parser.add_argument('-sc_s', '--scintillator_size', type=str, help='select scintillator size',
                        default='6mm3')
    parser.add_argument('-so', '--source', type=str, help='select radioative source ',
                        default='Cf252')
    parser.add_argument('-sa', '--samples', type=str, help='number of samples collected in data set ',
                        default='3e7samples')
    parser.add_argument('-out', '--outDir', type=str, help='Location to save the list of passed files ',
                        default='/home/bboxer/NeutronCamera/asic-neutron-camera/')
    parser.add_argument('-rm', '--removeCorrupt', type=boolean, help='When true, remove corrupted files ',
                        default=False)
    
    args = parser.parse_args()
    print(args, flush = True)
    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)
    
    return args

def main(args):

    ##Get the filename
    data_files = "{0}pickled_data/{1}_{2}_CH19FB_{3}_Data_3e7samples/{3}_Thresh_{5}_2us_SiPM_Bias_{4}V".format(args.directory,args.scintillator,args.scintillator_size,args.source,args.bias,args.threshold)

    #Use glob to make a list of all files with extension 
    filenames = glob.glob(data_files+"*.pki")
    if len(filenames) < 1:
        print('No files found. Check inputs\n\nEND\n\n',  flush = True)
        exit(0)
    
    #Get a list of files that have previously passed this check
    pass_file = '{}/{}_{}_{}_{}_{}_xml_passed.txt'.format(args.outDir, args.scintillator, args.scintillator_size, args.source, args.threshold, args.bias)
    
    #If there is a previous pass list of files, remove these files from the list
    if Path(pass_file).exists():
        with open(pass_file) as f:
            pass_list = f.read().splitlines() 
        #Remove files that have already passed from the list
        filenames = [a for a in filenames if a not in pass_list]
    
    ##Check that there are any files that are not in the pass list. If no files, end
    if len(filenames) < 1:
        print('All files correctly processed\n\nExiting\n\n',  flush = True)
        exit()
    
    passed = []
    failed = []

    #Iterate over files
    for f_idx, f in enumerate(filenames):
        print("Starting file {}/{}".format(f_idx+1,len(filenames)))

        #Try to load the pki file
        try:
            #Read the file
            temp_df=pd.read_pickle(f)
            
            #If number of events is not enough, class as failed
            if len (temp_df) < 99990:
                failed.append(f)
            #else class as passed
            else:
                passed.append(f)
            del temp_df
            gc.collect() #free up memory 
            pass
        #Most liekly exception is truncated pickle files causing failure to read
        except Exception as e:
            print("Corrupted file: ", e,  flush = True)
            #append to list of files that failed
            failed.append(f)

    ##Store the list of passed files, so when re-running script they can be skipped
    if len(passed) > 0:
        if not Path(pass_file).exists():
            with open(pass_file, 'w') as f:
                f.write('\n'.join(passed))   
        else:
            with open(pass_file, "a+") as f:
                f.write('\n')
                f.write('\n'.join(passed))

    #If arguement to remove corrupted file is selected, delete these files
    if len(failed) > 0 and args.removeCorrupt == True:
        for f in failed:
            print('\n Deleting corrupted file: ',f,  flush = True)
            pathlib.Path(f).unlink()


if __name__ == '__main__':
    args = parse_arguments()
    main(args)

print('\n\nEND\n\n',  flush = True)