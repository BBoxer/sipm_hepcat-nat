#!/usr/bin/env python3.9.5
# File name: process_xml_data.py
# Description: Process the trace information form the xml files in to either pki/h5 format
# Author: Billy Boxer (bboxer@ucdavis.edu)
# Date: 30-08-21

import sys
sys.path.append("../..") # Adds higher directory to python modules path.
from asic_utils import asic_utils as au
import argparse
import logging
from logging import critical, error, info, warning, debug
import re
import glob
import multiprocessing as mp
def parse_arguments():
    """Read arguments from a command line."""

    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')
    parser.add_argument('-d', '--directory', type=str, help='directory where data directories are held',
                        default='/group/tripathilab/projects/neutron_camera/data_DT5751/')
    parser.add_argument('-d_d', '--data_dir', type=str, help='directory containing data files',
                        default= 'Stilbene_6mm3_Ch19FB_Co57_Data_1e6samples')
    parser.add_argument('-o_t', '--output_type', type=str, help='file extension of the output data files when saving',
                        default='.pki')
    parser.add_argument('-b', '--bias', type=int, help='select a single bias to run over, default runs over all',
                        default='-1')
    parser.add_argument('-t', '--threshold', type=int, help='select a single threhsold to run over, default runs over all',
                        default='-1')
    parser.add_argument('-f', '--file_list',  nargs='+',type=str, help='the file to process',
                        default=None)                                                                                                                             
    args = parser.parse_args()
    print(args, flush = True)
    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)
    
    return args

def main(args):
    #Run function to convert this acquisitions xml files
    print('\n\nStarting: process_xml_data.py\nUsing: {}'.format(args), flush=True)
    #print('File directory: {}{}'.format(args.directory,args.data_dir), flush=True)
    directory = args.directory

    #    for f in args.file_list:
    #        print(f)

    #If a list oif files has been given as an argument, the code will just run on those files and exit
    if args.file_list != None:
        if len(args.file_list) > 0:
            pool = mp.Pool(mp.cpu_count())
            print("Number of CPU: ", pool)
            output_dir=directory+"/pickled_data/"+args.data_dir
            pro = [pool.apply( au.convert_to_pki_parallel, args=(directory,args.data_dir, output_dir, filename)) for filename in args.file_list ] 
            pool.close()
            exit()

    #Use glob to make a list of all xml files with extension 
    filenames = (glob.glob(directory + 'xml_files/' + args.data_dir+ '/*'))

    #Cut the list to selected bias
    if args.bias != -1:
        filenames=[ x for x in filenames if str(args.bias)+'V' in x ]
    #Cut the list to selected threshold
    if args.threshold != -1:
        filenames=[ x for x in filenames if 'Thresh_'+str(args.threshold) in x ]
    
    if args.bias != -1 or args.threshold != -1:
        print("Total files: ",len(filenames), flush=True)
        file=True
    else:
        file=False
    
    #If file list is empty then exit
    if len(filenames)<1:
            print('data files not found.\n EXITING', flush=True)
            exit()
    
    if args.output_type == '.h5' or args.output_type == 'HDF5' or args.output_type == 'hdf5':
        #output_dir='/'.join(re.split('/',args.directory)[:-1])+"/hdf5_data/"+args.data_dir
        output_dir=directory+"/hdf5_data/"+args.data_dir
        key='processed_data'
        au.convert_to_hdf5(args.directory, args.data_dir, output_dir, key)

    elif args.output_type == 'Pickle' or args.output_type == 'pickle' or args.output_type == '.pki':
        #output_dir='/'.join(re.split('/',args.directory)[:-1])+"/pickled_data/"+args.data_dir
        output_dir=directory+"/pickled_data/"+args.data_dir
        if file:
            au.convert_to_pki(directory, args.data_dir, output_dir, file=file, filenames=filenames)
        else:
            au.convert_to_pki(directory, args.data_dir, output_dir)
        
    else:
        print('Please provide valid output file type..\nExiting...', flush=True)
    
if __name__ == '__main__':
    args = parse_arguments()
    main(args)
