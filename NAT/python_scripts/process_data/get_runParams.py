#!/usr/bin/env python3.9
# File name: get_runParams.py
# Description: get the waveform parameters from the given set of aquisition files
# Author: Billy Boxer (bboxer@ucdavis.edu)
# Date: 30-08-21
#Updated: to include FOUT and alter the way the way the SOUT peak is saved (March 2022, Billy Boxer)
import sys
from unicodedata import category
sys.path.append("../..") # Adds higher directory to python modules path.
import logging
from logging import critical, error, info, warning, debug
import argparse
from asic_utils import asic_utils as au
from asic_utils import plot_utils as pu
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

import re
import peakutils
import scipy
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy.signal.wavelets import cascade
import math

import lxml.etree as etree
import textwrap
import io
import time
import pickle
from pathlib import Path
import glob
import random
import warnings

import gc

#import tracemalloc ##used to track tge memory usage during execution of the code. Be aware this increases the runtime and processing by about 30%

def parse_arguments():
    """
    Read arguments from a command line.
    
    :return: the user selected (or default) input arguements
    :rtype: <class 'argparse.Namespace'>
    """

    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')
    parser.add_argument('-d', '--directory', type=str, help='directory where data directories are held',
                        default='/group/tripathilab/projects/neutron_camera/data/')
    parser.add_argument('-d_d', '--data_dir', type=str, help='directory containing data files',
                        default= 'Stilbene_6mm3_Ch19FB_Co57_Data_1e6samples')
    parser.add_argument('-d_t', '--data_type', type=str, help='file extension of the data files',
                        default='.pki')
    parser.add_argument('-o_t', '--output_type', type=str, help='file extension of the output data files when saving',
                        default='.pki')
    parser.add_argument('-b', '--bias', type=int, help='select a single bias to run over, default will cause code to exit',
                        default='-1')
    parser.add_argument('-t', '--threshold', type=str, help='select a single threhsold to run over,  default will cause code to exit',
                        default='-1')
    parser.add_argument('-ch', '--channel', type=str, help='select which channel traces to process ("FOUT", "SOUT", or "Both")',
                        default='Both')
    parser.add_argument('-r', '--replace', type=bool, help='boolean to overwrite exisiting runParam file, default to False. Using -r followed by anything will set to True',
                        default=False)
    parser.add_argument('-ra', '--range', type=int, nargs='*', help='Either -1 for all files or a 2-tuple specifying min and max file index, ex: 1 10', 
                        default='-1')
    # parser.add_argument('-n', '--maxNum', type=int, help='The number of processed raw files to combine into one output runParam file', 
    #                     default='-1')

    args = parser.parse_args()
    print("\n{}\n".format(args), flush=True)

    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)
    
    return args


def find_nearest(array,value):
    """
    Find nearest value in an array

    :param array: the array of values to search
    :type array: array
    :param value: the value to find
    :type value: float
    :return: nearest value in array to given value 
    :rtype: float
    """
    
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
        return array[idx-1]
    else:
        return array[idx]

def get_riseTime(xdata,ydata,riseBounds,baseline,rms,maxAmp):
    """
    Determine the rise time by fitting a polynomial to the rising edge and finding the time between 10% and 90% of the amplitude

    :param xdata: the x data to fit
    :type xdata: array or list
    :param ydata: the y data to fit
    :type ydata: array or list
    :param riseBounds: the estimated bounds of the fit
    :type riseBounds: list
    :param baseline: the baseline of the waveform
    :type baseline: float
    :param rms: the noise rms of the waveform
    :type rms: float
    :param maxAmp: the maximum amplitude of the waveform
    :type maxAmp: float
    :return: the estimated rise time from 10% to 90% of the max amplitude
    :rtype: numpy.poly1d

    """
    
    #pass fail marker for checking h_rise is greater than l_rise
    pf = 0

    #prevent the poly fit from producing warning messages
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', np.RankWarning)
        rise_fit = np.polyfit(xdata[riseBounds[0]:riseBounds[-1] + 1], ydata[riseBounds[0]:riseBounds[-1] + 1] -baseline -rms, 2)
        rise_p1 = np.poly1d(rise_fit)
    

    #Make the xspace covering the bound region and determine the yspace using the poly fit
    xspace = np.linspace(xdata[riseBounds[0] - 2],xdata[riseBounds[-1] + 1],100)
    yspace = rise_p1(xspace)

    #try get the time corresponding to where the function is at 90% and 10% of the peak amplitude    
    try:
        h_rise = xspace[np.argwhere(yspace >= 0.9 * maxAmp)[0][0]]
        l_rise = xspace[np.argwhere(yspace <= 0.1 * maxAmp)[-1][-1]]
    
    #If there is an index error, assign a hold value
    except IndexError:
        h_rise = -99
        l_rise = 0
        pf = 1

    #if the h rise time is lower than the low there is an issue
    if h_rise < l_rise :
        if h_rise != -99:
            #print("Issue:\n\t {} - {} ={}".format(h_rise,l_rise,h_rise -l_rise))
            pf = 1
    
    #print('Rise time ', h_rise, l_rise, h_rise - l_rise)
    return(rise_p1, h_rise - l_rise, pf)
    
def get_rms(args):
    """
    Load in the rms data
    
    :param args: the input arguments to the script
    :type args: <class 'argparse.Namespace'>
    :return: the rms value
    :rtype: float
    """
    
    #Get the noise RMS data
    noise_files='Dark_Count_Ch19FB_Data'
    if not Path(args.directory+'/extracted_noise_params/'+noise_files+'_noiseRMSdict.pki').exists():
        if not Path('/group/tripathilab/projects/neutron_camera/data/extracted_noise_params/'+noise_files+'_noiseRMSdict.pki').exists():
            print('No noise file. Exiting', flush=True)
            exit()
        else:
            noiseRMS = pd.read_pickle('/group/tripathilab/projects/neutron_camera/data/extracted_noise_params/'+noise_files+'_noiseRMSdict.pki')
    else:
        noiseRMS = pd.read_pickle(args.directory+'/extracted_noise_params/'+noise_files+'_noiseRMSdict.pki')
        
    #Get rms values from noise dataFrame
    ##NEEDS CHECKING
    rms = noiseRMS[str(int(args.bias))][3][0]
    rms_error = noiseRMS[str(int(args.bias))][3][1]
    return(rms)

def get_pulseProps(event_traces=None,channel=None,bias=None,threshold=None, rms=None, sampleRes=None):
    """
    Function to process the properties of the traces. Process depends on if trace is SOUT or FOUT. 
    
    :param event_traces: the sout and fout traces for the event
    :type event_traces:
    :param channel: select whether to get properties for SOUT or FOUT
    :type channel: str
    :param bias: the bias of the aquistion
    :type bias: int
    :param threshold: the trheshold of the aquisiton
    :type threshold: int
    :param rms: the noise RMS of the data
    :type: float
    """

    #Set the index reference of the channel
    idx_channel = 0 if channel == "SOUT" else 1
    
    #Find baseline across the whole of the trace
    baseline = np.mean(peakutils.baseline(np.array((event_traces['trace'][idx_channel][:40])), 2)) #[mV]

    #Find the peak of the pulses as well as their widths and FWHMs
    peak_promY = 1.5 * bias / 29 if channel =='SOUT' else  threshold + baseline + 4 * rms#define the prominance based off the bias  
    peak_hgt = baseline + 4 * rms  if channel == 'SOUT' else baseline + 4 * rms #defines the minimum height of the peak based of the rms and baseline
    peak_dist = 1 if channel == 'SOUT' else 20 #defines the minimum allowed number of samples between peaks         
    peak, w, w2 = au.get_peakInfo(event_traces['trace'][idx_channel], peak_promY, peak_hgt, peak_dist) if channel =='SOUT' else au.get_peakInfo(event_traces['trace'][idx_channel][:100], peak_promY, peak_hgt, peak_dist)

    #Tag whether the multiple or zero peaks were found in the trace 
    #    multipeak_event = True if len(peak) > 1 else False #redundant
    zeropeak_event = True if len(peak) == 0 else False

    #If there are peaks in the trace
    if not zeropeak_event:
        #Determine amplitude of pulse(s)
        amp = [event_traces['trace'][idx_channel][p] - baseline for p in peak] #[mV]

        #If there are mulitple peaks
        if len(peak) > 1:

            #If these peaks have the same amplitude, take the first pulse as true pulse
            if len(np.argwhere(amp == np.amax(amp))) > 1:
                #Either find the peaks with the largest amplitude and then select the second
                #                peak = peak[np.argwhere(amp == np.amax(amp))[0]]
                #Or find the peak nearest to the 60th sample.
                peak = find_nearest(peak,60)# hardcode, find nearest to the 60th sample, this is specific to the digitizer
                
            #Else take the pulse with the max amplitude
            else: 
                peak = peak[np.argmax(amp)]

            #Set amplitude to single value
            amp = np.amax(amp) #[mV]
        #Else one peak, take the amplitude
        else:
            amp = amp[-1] #[mV]

        #Single peaks give np.ndarray and mulitpeaks give list, this is just to force it to be a single int value
        peak = peak[-1] if type(peak) == list or isinstance(peak, np.ndarray) else peak

        #Get the start of the trace
        low_sample = np.argwhere(np.asarray(event_traces['trace'][idx_channel][:peak] - baseline < 4 * rms))[-1][-1]
        high_sample = peak

    #Else, if zero peaks in trace
    else:
        amp = None
        low_sample = None
        high_sample = None
    return(baseline,amp,peak,[low_sample,high_sample],zeropeak_event)

def string_match(string1, string2):
    """
    Check if two strings match, or a string is in a list of strings (One variable must be a string). Case insensitive 
    
    :param string1: the first string for comparison
    :type string1: list or str
    :param string2: the second string for comparison
    :type string2: list or str
    :return: True if match
    :rtype: boolean
    """

    if type(string2) == str and type(string1) == list:
        string1, string2 = string2, string1

    try:
        if type(string2) == str:
            return(True if string1.casefold() == string2.casefold() else False)
        else:
            return(True if string1.casefold() in [s.casefold() for s in string2] else False)    
    except AttributeError as err:
        print('Error raised:', err)
        print('Functions requires either two strings (str, str), or a string and a list of stings (str, list[str])')
        print('string1: {}, type = {}\nstring2: {}, type = {}'.format(string1, type(string1), string2, type(string2)))
        exit()


def add_channel_to_plot(subplot, plot_row, plot_title, xdata, ydata, baseline, rms, peak, riseBounds, rise_p1):
    """
    Function to plot the trace, peak, baseline and rise bounds of a given trace
    
    :param subplot: the subplot object to add plots to
    :type subplot: <class 'asic_utils.plot_utils.general_plotter'> 
    :param plot_row: the row of the subplot object to add current plot
    :type plot_row: int
    :param plot_title: the title of the plot:
    :type plot_title: str
    :param xdata: the xdata to plot
    :type xdata: array or list
    :param ydata: the ydata to plot
    :type ydata: array or list
    :param baseline: the baseline of the waveform
    :type baseline: float
    :param rms: the noise RMS of the waveform
    :type rms: float
    :param peak: the sample corresponding to the peak of the waveform
    :type peak: int
    :param riseBounds: the sample values bounding the fit to the rising edge
    :type riseBounds: list[int]
    :param rise_p1: the poly fit to the rising edge of the waveform
    :type rise_p1: numpy.poly1d

    """
    #plot the baseline subtracted trace
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'scatter', xdata = xdata, ydata = ydata - baseline -rms, plt_label = 'Trace') 
    #Plot the peak
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'scatter', xdata = xdata[peak], ydata = ydata[peak] - baseline -rms, color='red', marker ='x',s_size=40,plt_label =  'Peak')

    #Plot the bounds on the rise time
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'axhline',  ydata = 0.9 * (ydata[peak] - baseline), plt_label =  'Peak Amplitude')
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'axhline',  ydata = -rms, plt_label =  '-ve rms')
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'axhline',  ydata = rms, plt_label =  '+ve rms')

    #Add plot properties
    subplot.add_subplot_properties(index_row = plot_row, index_col = 0, xlim = [0,300], xlabel = 'Time [ns]', ylabel = 'Amplitude [mV]', title = plot_title)
    subplot.add_subplot_plot(index_row = plot_row, index_col = 0, plotType = 'plot', xdata = xdata[riseBounds[0]:riseBounds[-1] + 1], ydata = rise_p1(xdata[riseBounds[0]:riseBounds[-1] + 1]), plt_label = 'fit to rise') 

    
def main(args, plotOutput, save_runParams, overwrite_outDir=False, debug=False):
    """
    Main function of the code to produce RQs for each waveform

    :param args: the input arguments to the script
    :type args: <class 'argparse.Namespace'>
    :param plotOutput: select whether or not to plot the waveforms
    :type plotOutput: boolean
    :param save_runParams: toggle to save the RQs
    :type save_runParams: boolean
    :param overwrite_outDir: bootstrap overwriting the autogenerated output directory
    :type overwrite_outDir: boolean
    :param debug: toggle debug plots
    :type debug: boolean
    """
    
    #tracemalloc.start() #malloc test ##used to track memory usage
    #current_mb =[] #malloc test ##used to track memory usage
    #peak_mb = []   #malloc test ##used to track memory usage

    #Define fixed value variables, counters, lists to poulate and dataFrames to populate 
    global ns #global variable as cba passing to functions
    ns = 1e-9 #define nanosecond
    fail = 0 #Counter for failed events
    fail_Zero = 0
    fail_SOUT = 0
    riseIssue_SOUT = 0
    riseIssue_FOUT =0

    #Add to the directory path the directory containing the data for a given extension 
    #Should be agnostic to data type
    if args.data_type == '.pki':
        directory = args.directory+"pickled_data/"
    elif args.data_type == '.xml':
        directory =args.directory+ "xml_files/"
    elif args.data_type == '.h5':
        directory = args.directory+"hdf5_data/"
    #Check the directory exisits
    if not Path(directory).exists():
        print("data_dir not found: {}\n EXITING".format(directory), flush=True)
        exit()
        
    #Use glob to make a list of all files with extension 
    filenames = (glob.glob(directory + args.data_dir + '/*' + args.data_type))
    #print('Filenames with no cuts: {}'.format(filenames), flush=True)

    #Cut the list to selected bias
    if args.bias != -1:
        filenames=[ x for x in filenames if str(args.bias)+'V' in x ]
        #print('Filenames with bias cuts: {}'.format(filenames), flush=True)

    #Cut the list to selected threshold
    if args.threshold != -1:
        filenames=[ x for x in filenames if 'Thresh_'+str(args.threshold) in x ]
        #print('Filenames with threshold cuts: {}'.format(filenames), flush=True)

    #Cut the list to selected index range
    #print("Current value of args.range is: {}".format(args.range), flush=True)
    if args.range[0] != -1:
        filenames_index=[]
        for i in range(args.range[0], args.range[1]+1):
            for x in filenames:
                if 'V_{}_'.format(i) in x:
                    filenames_index.append(x)
        filenames=filenames_index
        del filenames_index
        #print('Filenames with bias, threshold and range cuts: {}'.format(filenames), flush=True)

    #If file list is empty then exit
    if len(filenames)<1:
        print('data files not found.\n EXITING', flush=True)
        exit()

    print("Total files: ",len(filenames), flush=True)
    print("Files to Process: {}".format(filenames), flush=True)

    #Name the output directory and output file                                                                                                                                                           
    dir_runParams = args.directory+"runParams_v2/"+args.data_dir if not overwrite_outDir else "/group/tripathilab/projects/neutron_camera/data/runParams_v2/"+args.data_dir
    
    df_runParams = pd.DataFrame() #Store the important features of each run
    multipeak_event = [] #Store for events with multiple peaks
    rms = get_rms(args)
    
    #Start of loop over each file
    for filename in filenames:
        file_index = (re.split('_', filename)[-3])
        print("\n\nStart of processing: ",filename,flush=True)
        #current, peak = tracemalloc.get_traced_memory()    #malloc test ##used to track memory usage
        #print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")   #malloc test ##used to track memory usage
        
        #Set resistance in circuit by board name
        resistor = 50 if 'FB' in filename else 10
        
        #Get the information about the run and the events
        try:
            bias, sampleLength, thresh, sampleFreq, sampleRes, df_properties = au.load_Data(filename, directory + args.data_dir)
        except NameError:
            print('Unable to load data in format {}'.format(filename), flush=True)
            continue
        output_handle = 'Bias_{}'.format(args.bias) if args.bias != -1 else 'Bias_all'
        output_handle += '_Thresh_{:2.2f}'.format(au.adcc_to_mV(args.threshold, sampleRes)) if args.threshold != -1 else '_Thresh_all'
        output_handle += '_Index_{}_{}'.format(args.range[0],args.range[1]) if args.range[0] != -1 else '_Index_all'
        output_file='{}/{}_{}{}'.format(dir_runParams, args.data_dir, output_handle, args.output_type)
        print('Output runParams file name: {}'.format(output_file), flush=True)
        #Check if the output file exists, if so, if overwrite is false then exit      
        if Path(output_file).exists():
            if not args.replace:
                print('\nFile:\t{} already exists. Moving to next file'.format(output_file),flush=True)
                exit()
 
        #for e in random.sample(range(1, len(df_properties['event'].unique())), 500):
        for e in df_properties['event'].unique():#
            if e%10000 == 0:
                print("Currently processing event {}".format(e), flush=True)
                #current, peak = tracemalloc.get_traced_memory() #malloc test ##used to track memory usage
                #print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")   #malloc test ##used to track memory usage
            
            iii = 1
            if iii == 1:
            #try:
                #Set the trace to just those for this event
                event_traces = (df_properties[df_properties['event'] == e]).reset_index() # divide by resistor here
                xspace = np.linspace(1, len(event_traces['trace'][0]), len(event_traces['trace'][0])) * (1 / sampleFreq) #Define an x variable for plotting and area calculation

                #Only run the following is channel is 'FOUT' or 'Both'
                if string_match(args.channel, ['FOUT', 'Both']):
                    FOUT_baseline, FOUT_amp, FOUT_peak, FOUT_riseBounds, FOUT_zeropeak = get_pulseProps(event_traces, channel = 'FOUT', bias =  args.bias, threshold = thresh, rms = rms)

                    #FOUT area        
                    try:
                        FOUT_area = scipy.integrate.trapz(event_traces['trace'][1][FOUT_peak - 10:FOUT_peak + 10] - FOUT_baseline, xspace[FOUT_peak - 10:FOUT_peak + 10]) #[mV s]
                    except:
                        FOUT_area = -100
                        #print('FOUT error:\n\t No peak found, length =',len(FOUT_peak))
                    
                    #If there are zero peaks in trace, continue to next event
                    if FOUT_zeropeak == True:
                        #print('FOUT fail, ',len(FOUT_peak), e, flush = True)
                        fail += 1
                        fail_Zero += 1
                        continue
                    
                    #Determine the t0 of the FOUT pulse, currently just taken as the peak of the pulse, could use low sample
                    FOUT_T0 = FOUT_peak * 1/sampleFreq / ns #[s] np.argwhere(event_traces['trace'][1][:FOUT_peak} > thresh
                    
                    ##Get the FOUT rise time
                    FOUT_rise_p1, FOUT_rise, pf= get_riseTime(xspace / ns, event_traces['trace'][1],FOUT_riseBounds,FOUT_baseline,rms,event_traces['trace'][1][FOUT_peak] - FOUT_baseline - rms)
                    if pf ==1:
                        riseIssue_FOUT += 1
                        FOUT_rise = -99
                    
                #Only run the following if channel is 'SOUT' or both
                if string_match(args.channel, ['SOUT', 'Both']):
                    SOUT_baseline, SOUT_amp, SOUT_peak, SOUT_riseBounds, SOUT_zeropeak = get_pulseProps(event_traces, channel = 'SOUT', bias = args.bias, threshold = args.threshold, rms = rms)
                    if SOUT_zeropeak == True:
                        #print('SOUT fail, ',len(SOUT_peak), e, flush = True)
                        fail += 1
                        fail_SOUT += 1
                        continue
                    
                    #SOUT rise and area
                    SOUT_area = scipy.integrate.cumtrapz(event_traces['trace'][0][:] - SOUT_baseline, xspace[:]) #[mV s]
                    SOUT_rise_p1, SOUT_rise, pf = get_riseTime(xspace / ns, event_traces['trace'][0],SOUT_riseBounds,SOUT_baseline,rms,event_traces['trace'][0][SOUT_peak] - SOUT_baseline - rms)
                    
                    if pf ==1: 
                        riseIssue_SOUT += 1
                        SOUT_rise = -99

                if plotOutput:
                    #create plot object with nrows = number of channels
                    trace_subplot = pu.general_plotter(nrow = 1 if not string_match(args.channel,'Both') else 2, ncol = 1)
                    #Only run the following if channel is 'SOUT' or 'Both'
                    if string_match(args.channel, ['SOUT', 'Both']):
                        #Set the row for the plot (0 always for SOUT)
                        plot_row = 0
                        plot_title =  'SOUT {}V {:2.2f}mV (File:{}, Event:{})'.format(args.bias, au.adcc_to_mV(args.threshold, sampleRes), file_index, e)

                        #if no peak was found, set to arb values
                        if SOUT_zeropeak:
                            SOUT_peak = [0]
                            SOUT_riseBounds = [0, 1]

                        #make plot using function
                        add_channel_to_plot(trace_subplot, plot_row, plot_title, xspace / ns, event_traces['trace'][0], SOUT_baseline, rms, SOUT_peak, SOUT_riseBounds, SOUT_rise_p1)
                            
                    if string_match(args.channel, ['FOUT', 'Both']):
                        #Set the row for the plot ) if FOUT only, 1 if Both
                        plot_row = 1 if string_match(args.channel, 'Both') else 0
                        plot_title = 'FOUT {}V {:2.2f}mV (File:{}, Event:{})'.format(args.bias, au.adcc_to_mV(args.threshold, sampleRes), file_index, e)
                        
                        #if no peak was found, set to arb values                        
                        if FOUT_zeropeak:
                            FOUT_peak = [0]
                            FOUT_riseBounds = [0, 1]

                        #make plot using function
                        add_channel_to_plot(trace_subplot, plot_row, plot_title, xspace / ns, event_traces['trace'][1], FOUT_baseline, rms, FOUT_peak, FOUT_riseBounds, FOUT_rise_p1)
                    #save the plot
                    trace_subplot.export(save = True, filename = 'trace_plot_{}_{}_{}.png'.format(args.data_dir, e, args.channel))
                        
                #Populate a dataframe with pulse properties               
                if save_runParams or debug:
                    #If channel is 'SOUT' or 'Both' make a dataframe of the SOUT parameters
                    if string_match(args.channel, ['SOUT', 'Both']):
                        #turn the sout area from volts to amps and from seconds to nano seconds and convert to a float16 to reduce memory on saving
                        df_temp_s = pd.DataFrame([[bias, thresh, file_index, e, SOUT_baseline, SOUT_peak, SOUT_amp/resistor, np.asarray([np.float16(amp_mV/resistor/ns) for amp_mV in SOUT_area]), SOUT_rise]],
                                   columns=['bias_V', 'threshold_mV', 'file_index', 'event', 'SOUT baseline_mV',
                                        'SOUT peak', 'SOUT peakAmplitude_mA', 'SOUT area_mA_ns', 'SOUT rise_ns'], index = None)
                        df_temp_s = df_temp_s.astype({"bias_V": 'float16', "threshold_mV": 'float16', 'file_index':'int16', 'event':'int32',
                                'SOUT baseline_mV':'float16', 'SOUT peak':'float16', 'SOUT peakAmplitude_mA':'float16', 'SOUT rise_ns':'float16', 'SOUT area_mA_ns':'object'}, copy=False)

                        #If only SOUT, append to the runParams dataframe
                        if string_match(args.channel, 'SOUT'):
                            df_runParams = df_runParams.append(df_temp_s)
                            del df_temp_s
                          
                    #If channel is 'FOUT' or 'Both' make a dataframe of the FOUT parameters                              
                    if string_match(args.channel, ['FOUT', 'Both']):
                        df_temp_f = pd.DataFrame([[bias, thresh, file_index, e, FOUT_baseline, FOUT_peak, FOUT_amp, FOUT_area/ns, FOUT_rise, FOUT_T0]],
                                    columns=['bias_V', 'threshold_mV', 'file_index', 'event', 'FOUT baseline_mV',
                                            'FOUT peak', 'FOUT peakAmplitude_mV','FOUT area_mV_ns', 'FOUT rise_ns', 'FOUT T0_ns'], index = None)
                        df_temp_f = df_temp_f.astype({"bias_V": 'float16', "threshold_mV": 'float16', 'file_index':'int16', 'event':'int32',
                                'FOUT baseline_mV':'float16', 'FOUT peak':'float16', 'FOUT peakAmplitude_mV':'float16', 'FOUT area_mV_ns':'float16', 'FOUT rise_ns':'float16', 'FOUT T0_ns':'float16'}, copy=False)
                         

                        #If only FOUT, append to the runParams dataframe
                        if string_match(args.channel, 'FOUT'):
                            df_runParams = df_runParams.append(df_temp_f)
                            del df_temp_f
                            
                        #If both, merge the dataframes and then append to runParams
                        elif string_match(args.channel, 'Both'):
                           
                            df_runParams = df_runParams.append(pd.merge(df_temp_f,df_temp_s,copy=False))
                            del df_temp_s, df_temp_f
                            
            #except:
                #fail+=1
                #print("Total number of events failed is now: {}".format(fail), flush=True)
            
        #delete the event dataframe
        del event_traces
        print(df_runParams.info())
        #current, peak = tracemalloc.get_traced_memory()    #malloc test ##used to track memory usage
        #print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")   #malloc test ##used to track memory usage
        #current_mb.append(current / 10**6) #malloc test ##used to track memory usage
        #peak_mb.append(peak / 10**6)   #malloc test ##used to track memory usage

    #Clean the index of the dataframe
    df_runParams=df_runParams.reset_index()
    del df_runParams['index'], df_properties
    #current, peak = tracemalloc.get_traced_memory()    #malloc test ##used to track memory usage
    #print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")   #malloc test ##used to track memory usage

    gc.collect() #free up memory 

    #print the head of the dataframe
    print(df_runParams.head(), flush = True)
    print("\n\n")
    print(df_runParams.info(), flush = True)
    print("\n")
    print(df_runParams.memory_usage(deep=True), flush = True)
    
    if plotOutput or debug:
        Path('plots/QC_runParams/').mkdir(parents=True, exist_ok=True)
        
        if string_match(args.channel, ['FOUT', 'Both']):
            FOUT_subplot = pu.general_plotter(nrow = 2 , ncol = 2)
            FOUT_subplot.add_subplot_plot(index_row = 0, index_col = 0, plotType = 'hist', ydata = df_runParams['FOUT peakAmplitude_mV']) 
            FOUT_subplot.add_subplot_properties(index_row = 0, index_col = 0, xlabel = 'Amplitude [mV]', ylabel = 'Counts', title = 'FOUT peak amplitude')
            
            FOUT_subplot.add_subplot_plot(index_row = 1, index_col = 0, plotType = 'hist', ydata = df_runParams['FOUT rise_ns'], binRange=(0,100), numBins=101) 
            FOUT_subplot.add_subplot_properties(index_row = 1, index_col = 0, xlabel = 'Time [ns]', ylabel = 'Counts', title = 'FOUT rise time')
            
            FOUT_subplot.add_subplot_plot(index_row = 0, index_col = 1, plotType = 'hist', ydata = df_runParams['FOUT T0_ns']) 
            FOUT_subplot.add_subplot_properties(index_row = 0, index_col = 1, xlabel = 'Time [ns]', ylabel = 'Counts', title = 'FOUT T0')
            
            FOUT_subplot.add_subplot_plot(index_row = 1, index_col = 1, plotType = 'hist', ydata = df_runParams['FOUT area_mV_s']) 
            FOUT_subplot.add_subplot_properties(index_row = 1, index_col = 1, xlabel = 'Area [mAs]', ylabel = 'Counts', title = 'FOUT Area')
            
            FOUT_subplot.export(save = True, filename = 'plots/QC_runParams/QC_runParams_FOUT_{}_Bias{}V_Tresh{}_{}.png'.format(args.data_dir, args.bias, args.threshold,args.range))

        if string_match(args.channel, ['SOUT', 'Both']):
            rough_conversion = (1.602e-19 * au.get_gain(args.bias)*1e3)*1e9
            temp_sout_area  = []
            temp_psd = []
            ##QC check of SOUT
            SOUT_subplot = pu.general_plotter(nrow = 2, ncol = 2)
            
            SOUT_subplot.add_subplot_plot(index_row = 0, index_col = 0, plotType = 'hist', ydata = df_runParams['SOUT peakAmplitude_mA']) 
            SOUT_subplot.add_subplot_properties(index_row = 0, index_col = 0, xlabel = 'Amplitude [mA]', ylabel = 'Counts', title = 'SOUT peak amplitude') 

            SOUT_subplot.add_subplot_plot(index_row = 1, index_col = 0, plotType = 'hist', ydata = df_runParams['SOUT rise_ns'], binRange=(0,100), numBins=101) 
            SOUT_subplot.add_subplot_properties(index_row = 1, index_col = 0, xlabel = 'Time [ns]', ylabel = 'Counts', title = 'SOUT rise time')        

            for index, row in df_runParams.iterrows():
                temp_sout_area.append(row['SOUT area_mA_ns'][-1] / ns) 
                temp_psd.append(row['SOUT area_mA_ns'][100]/row['SOUT area_mA_ns'][400])
            SOUT_subplot.add_subplot_plot(index_row = 1, index_col = 1, plotType = 'hist', ydata = temp_sout_area) 
            SOUT_subplot.add_subplot_properties(index_row = 1, index_col = 1, xlabel = 'Area [mA$\cdot$ns]', ylabel = 'Counts', title = 'SOUT total area')        
         
            SOUT_subplot.export(save = True, filename = 'plots/QC_runParams/QC_runPArams_SOUT_{}_Bias{}V_Tresh{}_{}.png'.format(args.data_dir, args.bias, args.threshold,args.range))

            ##QC check of PSD
            SOUT_PSD = pu.general_plotter(nrow = 1, ncol = 1)
            SOUT_PSD.add_subplot_plot(index_row = 0, index_col = 0, xdata = [mA_ns*rough_conversion for mA_ns in temp_sout_area], ydata = temp_psd, plotType = 'hist2d',
                 binRange = ((0, np.max(temp_sout_area)*rough_conversion),(0, 1)), numBins = (1000,200), cbar_label='Counts',
                norm = matplotlib.colors.LogNorm())
            SOUT_PSD.add_subplot_plot(index_row = 0, index_col = 0,  ydata = 60, plotType = 'axvline', plt_label ='60 keV gamma')
            SOUT_PSD.add_subplot_properties(index_row = 0, index_col = 0, xlim=(0,np.max(temp_sout_area)*rough_conversion), xlabel = 'Energy [Arb.]', ylabel = 'PSD', title = ' SOUT PSD: {}'.format(args.data_dir))                
           
            SOUT_PSD.export(save = True, filename = 'plots/QC_runParams/QC_runPArams_PSD_{}_Bias{}V_Tresh{}_{}.png'.format(args.data_dir, args.bias, args.threshold,args.range))
        
        if string_match(args.channel, 'Both'):
            FOUT_amp_v_SOUT_area = pu.general_plotter(nrow = 1, ncol = 1)
            FOUT_amp_v_SOUT_area.add_subplot_plot(index_row = 0, index_col = 0, ydata = df_runParams['FOUT peakAmplitude_mV'], 
                xdata = [a*rough_conversion for a in temp_sout_area], plotType = 'hist2d',
                numBins = (100,100), cbar_label='Counts',
                norm = matplotlib.colors.LogNorm())
            FOUT_amp_v_SOUT_area.add_subplot_properties(index_row = 0, index_col = 0,
                xlabel = 'Peak Amplitude [mV]', ylabel = 'Area [mV$\cdot$ns]', title = 'FOUT Amplitude vs SOUT Area: {}'.format(args.data_dir))
                #ylim =(0.3, 0.6),xlim=(0,energy_bound[1]))
                #cbar_label='Counts')#,text_str = '')
            FOUT_amp_v_SOUT_area.export(save = True, filename = 'plots/QC_runParams/QC_runParams_FOUT_amp_v_SOUT_area_{}_Bias{}V_Tresh{}_{}.png'.format(args.data_dir, args.bias, args.threshold,args.range))



    #save the output file
    if save_runParams:
        #Check the output dir exists, if not make it
        Path(dir_runParams).mkdir(parents=True, exist_ok=True)
        
        #Save the data frame as a .pki for easier loading later
        print("Saving runParams to: {}".format(output_file), flush=True)

        #save to pki or h5
        if args.output_type =='.pki':
            df_runParams.to_pickle(output_file)

        elif args.output_type =='.h5':
            df_runParams.to_hdf(output_file, key='runParams')

         
    print('Final total number of failed events: {}'.format(fail), flush = True)
    print('SOUT fails: {}'.format(fail_SOUT), flush = True)
    print('FOUT ZERO peak fails: {}'.format(fail_Zero), flush = True)
    print("SOUT rise time issues: {}".format(riseIssue_SOUT), flush = True)
    print("FOUT rise time issues: {}".format(riseIssue_FOUT), flush = True)
    print('Total events: {}'.format(e), flush = True)
 
    #current, peak = tracemalloc.get_traced_memory()    #malloc test ##used to track memory usage
    #print(f"End of file: Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")  #malloc test ##used to track memory usage
    #plt.plot(current_mb, marker ='x')  #malloc test ##used to track memory usage
    #plt.savefig('current_mb__{}_{}_{}.png'.format(args.channel,args.range[0],args.range[1]))   #malloc test ##used to track memory usage
    #plt.clf()  #malloc test ##used to track memory usage
    #plt.plot(peak_mb, marker = 'x')    #malloc test ##used to track memory usage
    #plt.savefig('peak_mb.png_{}_{}_{}.png'.format(args.channel,args.range[0],args.range[1]))   #malloc test ##used to track memory usage
    #print(df_runParams.head(), flush = True)
    #print(df_runParams.tail(), flush = True)

if __name__ == '__main__':
    plotOutput = False #toggle plots of the traces
    debug = False #toggle print statements and QA plots
    save_runParams = True #toggle to save the run parameters to a .pki
    overwrite_outDir = False #toggle to overwrite the path of the output director for the RQ files

    #Timer for benchmarking
    start_time = time.time() 

    #Get the arguments from the parser
    args = parse_arguments()

    #Check bias and threshold have been input correctly
    if args.bias ==-1 or args.threshold == -1:
        raise ValueError('\nExplicit bias (set value {}) and threshold (set value {}) were not set\nExiting'.format(args.bias, args.threshold))
        exit()
    
    #Check if the selected channel is valid
    if not string_match(args.channel,['SOUT', 'FOUT', 'Both']):
        raise ValueError('\nIllegal Arguement entered for channel, {}\n\tTry: "SOUT", "FOUT", or "Both" (case insensitive)\nExiting'.format(args.channel))
        exit()
    
    main(args,plotOutput,save_runParams,overwrite_outDir,debug)
    
    print('\nRun time: {}\nEnd\n\n'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time))), flush = True)