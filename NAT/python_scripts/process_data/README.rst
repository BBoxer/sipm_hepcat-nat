Process Data
=============

Contain scripts for:
- Converting the xml format data from the scope to .pki format. This script can be submitted using "...."
- Process the data files in to a set of reduced quantity files. Produces a Pandas DataFrame with properties for SOUT and/or FOUT
