#!/bin/bash -l

if [[ $HOME == *"bboxer"* ]]; then
    EMAIL=bboxer@ucdavis.edu ##what email should slurm notifications be sent to
else
    EMAIL=jyjohnson@ucdavis.edu ##what email should slurm notifications be sent to
fi

sbatch <<EOT
#!/bin/bash -l
#SBATCH -D $1/asic-neutron-camera/
#SBATCH -J process_xml
#SBATCH -o $1/asic-neutron-camera/Phase1_Tests/slurm_log/out-%j_%N.txt
#SBATCH -e $1/asic-neutron-camera/Phase1_Tests/slurm_log/error-%j_%N.txt
#SBATCH -t 72:00:00
#SBATCH -N 1
# Send email notifications.  
#SBATCH --mail-type=ALL # other options are ALL, NONE, BEGIN, FAIL
#SBATCH --mail-user=$EMAIL
#SBATCH --partition=$6
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=4000
#SBATCH --cpus-per-task=4


module use $HOME/MyModules/
module load miniconda3/1.0

source $HOME/.bashrc
if [[ $1 == *"bboxer"* ]]
then
  conda activate asic_env
else
  conda activate py3env
fi


cd $1/asic-neutron-camera/Phase1_Tests/python_scripts/process_data/
python process_xml_data.py -d $5 -d_d $2 -b $3 -t $4
EOT
