#!/bin/bash -l

if [[ $HOME == *"bboxer"* ]]; then
    EMAIL=bboxer@ucdavis.edu ##which email should slurm notifications be sent to
elif [[ $HOME == *"jyjohn24"* ]]; then
    EMAIL=jyjohnson@ucdavis.edu ##which email should slurm notifications be sent to
elif [[ $HOME == *"rishita"* ]]; then
    EMAIL=rkhandwala@ucdavis.edu ##which email should slurm notifications be sent to
else
    echo 'User not recognised. Exiting' #If you have seen this error, just append your home user area and email to the if statement above
    exit 0
fi

if (( $# == 9 )); then
    RANGE="$7 $8"
    JOB_NAME="get_runParams_$3_$4_$7_$8"
elif (( $# == 7 )); then
    RANGE=-1
    JOB_NAME="get_runParams_$3_$4"
fi

sbatch <<EOT
#!/bin/bash -l
#SBATCH -D $1/asic-neutron-camera/
#SBATCH -J $JOB_NAME
#SBATCH -o $1/asic-neutron-camera/Phase1_Tests/slurm_log/out-%j.txt
#SBATCH -e $1/asic-neutron-camera/Phase1_Tests/slurm_log/error-%j.txt
#SBATCH -t 72:00:00
#SBATCH -N 1
# Send email notifications.  
#SBATCH --mail-type=ALL # other options are ALL, NONE, BEGIN, FAIL
#SBATCH --mail-user=$EMAIL
#SBATCH --partition=$6
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=4000
#SBATCH --cpus-per-task=4


module use $HOME/MyModules/
module load miniconda3/1.0

source $HOME/.bashrc
if [[ $1 == *"bboxer"* ]]; then
conda activate asic_env
elif [[ $HOME == *"jyjohn24"* ]]; then
conda activate py3env
elif [[ $HOME == *"rishita"* ]]; then
conda activate asic_env
else
conda activate asic_env
fi

cd $1/asic-neutron-camera/Phase1_Tests/python_scripts/process_data/
python get_runParams.py -d_d $2 -b $3 -t $4 -ra $RANGE -ch $9 -r True

EOT
