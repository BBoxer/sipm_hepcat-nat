#!/bin/bash -l
if [[ $HOME == *"bboxer"* ]]; then
    CODE_DIR=$HOME/NeutronCamera ##Name of directory containign asic-neutron-camera dir
else
    CODE_DIR=$HOME/projects/neutron_camera ##Name of directory containign asic-neutron-camera dir
fi
DIR=/group/tripathilab/projects/neutron_camera/data_DT5751/ #$CODE_DIR/data/ ##Name of general data directory

SCINT='Ej276'
SCINT_SIZE='6mm3'
SOURCE='Cf252'
SAMPLES='3e7samples'
BIAS_LIST=29 #bias of the data set to process
THRESH_LIST='0039`' # 0037 for Stilbene, 0039 for EJ276 6mm and 0040 for EJ276 50mm #threhold of data set to process
P=med #submission que priority
SCRIPT=check_xml_to_pki.sh #script to run

if [[ "$BIAS_LIST" == "all" ]]; then
    if [[ "$THRESH_LIST" == "all" ]]; then  
	sh echo $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/$SCRIPT $CODE_DIR $DIR $SCINT $SCINT_SIZE $SOURCE $SAMPLES -1 -1 $P
    else
	for thresh in "${THRESH_LIST[@]}"; do
	    sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/$SCRIPT $CODE_DIR $DIR $SCINT $SCINT_SIZE $SOURCE $SAMPLES -1 $thresh $P
	done
    fi
    
else
    if [[ "$THRESH_LIST" == "all" ]]; then
	for bias in "${BIAS_LIST[@]}"; do
	    sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/$SCRIPT $CODE_DIR $DIR $SCINT $SCINT_SIZE $SOURCE $SAMPLES $bias -1 $P
	done
    else
	 for bias in "${BIAS_LIST[@]}"; do
	     for thresh in "${THRESH_LIST[@]}"; do
		 sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/$SCRIPT $CODE_DIR $DIR $SCINT $SCINT_SIZE $SOURCE $SAMPLES $bias $thresh $P
	     done
	 done
    fi
fi
