#!/bin/bash -l
if [[ $HOME == *"bboxer"* ]]; then
    CODE_DIR=$HOME/NeutronCamera ##Name of directory containign asic-neutron-camera dir
else
    CODE_DIR=$HOME/projects/neutron_camera ##Name of directory containign asic-neutron-camera dir
fi
DIR=/group/tripathilab/projects/neutron_camera/data/ #$CODE_DIR/data/ ##Name of general data directory


DATA_DIR=Stilbene_6mm3_CH19FB_AmBe_Data_3e7samples #EJ276_50mm3_CH19FB_AmBe_Data_3e7samples ##Name of directory containing data to process
BIAS=29 #bias of the data set to process
THRESHOLD=8097 #8105 #threhold of data set to process
files_per_sub=4 #number of files in each submitted job
P=med #submission que priority

#To submit only one set of files when debugging
debug_subOne=true

declare -a file_array #Array to store lists of files

## Get the relevant files
files=($DIR"xml_files/"$DATA_DIR/*Thresh_${THRESHOLD}*Bias_${BIAS}V*.xml)

## Checks to see if the files arg contains wildcard(*). If so, no files found and exit
if [[ $files = *[*]* ]] ; then
    echo "No files found for given combination of: $DATA_DIR $BIAS $THRESHOLD"
    echo "Exiting"
    echo
    exit
fi

## Check if the xml has been process already, if so remove from the list fo files
for filename in "${files[@]}";do
    NAME=${DIR}pickled_data/$DATA_DIR/"$(basename ${filename} .xml)"_*
    if ls $NAME 1> /dev/null 2>&1; then
	    echo
	    echo "Skipping: $filename, as already processed"
    else
	    files_reduced+=($filename)
    fi
done

#Recreate file array from reduced list and unset the reduced list
echo
echo "Original number of files ${#files[@]}"
files=("${files_reduced[@]}")
unset files_reduced
echo "Reduced number of files ${#files[@]}"
echo


#Determine the end point of the loop for populating the file list array
END=$(( (${#files[@]} +$files_per_sub -1)/$files_per_sub))

#If all files have already been processed, exit
if [[ "$END" == 0 ]];then
    echo -e "All files have been already processed.\nNo jobs submitted\nExiting\n\n"
    exit
fi

echo "Creating $END sets of files of max length $files_per_sub"

## Split the data into files_per_sub and store as an index in an arrays 
for((i=0;i<=$END;i++));do
    file_array[$i]="${files[@]:$(( $i*$files_per_sub)):$files_per_sub}"
done

## Loop over each of the lists of files
for file_list in "${file_array[@]}"; do
        if [[ ${#file_list} != 0 ]]; then
            echo
            echo "Files to be submitted: ${file_list[@]}" 
            sh  $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/parallel_process_xml.sh $CODE_DIR $DATA_DIR $P $DIR $file_list 
           
            if [ "$debug_subOne" = true ]; then
                exit
            fi    
        fi
done