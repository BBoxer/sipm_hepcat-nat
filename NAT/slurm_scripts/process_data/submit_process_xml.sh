#!/bin/bash -l

if [[ $HOME == *"bboxer"* ]]; then
    CODE_DIR=$HOME/NeutronCamera ##Name of directory containign asic-neutron-camera dir
else
    CODE_DIR=$HOME/projects/neutron_camera ##Name of directory containign asic-neutron-camera dir
fi
DIR=$CODE_DIR/data/ ##Name of general data directory

##This script will convert each file that meets the following conditions to an output pickle file. It is a 1-to-1 mapping and it will check for the existence of the files before converting so there is no issue with letting BIAS_LIST and THRESH_LIST be "all"
##

DATA_DIR=EJ276_6mm3_CH19FB_AmBe_Data_1e6samples ##Name of directory containing data to process
BIAS_LIST=(29) #"all" #(29) ##List specific biases (## ## ##) to run over or select "all"
THRESH_LIST=(8090) #List the specific threshold (## ## ##) to run over or select "all"  
P=med

if [[ "$BIAS_LIST" == "all" ]]; then
    if [[ "$THRESH_LIST" == "all" ]]; then  
	sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/process_xml.sh $CODE_DIR $DATA_DIR -1 -1 $DIR $P
    else
	for thresh in "${THRESH_LIST[@]}"; do
	    sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/process_xml.sh $CODE_DIR $DATA_DIR -1 $thresh $DIR $P
	done
    fi
    
else
    if [[ "$THRESH_LIST" == "all" ]]; then
	for bias in "${BIAS_LIST[@]}"; do
	    sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/process_xml.sh $CODE_DIR $DATA_DIR $bias -1 $DIR $P
	done
    else
	 for bias in "${BIAS_LIST[@]}"; do
	     for thresh in "${THRESH_LIST[@]}"; do
		 sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/process_xml.sh $CODE_DIR $DATA_DIR $bias $thresh $DIR $P
	     done
	 done
    fi
fi
