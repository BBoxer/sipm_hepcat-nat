#!/bin/bash -l

if [[ $HOME == *"bboxer"* ]]; then
    CODE_DIR=$HOME/NeutronCamera ##Name of directory containing /asic-neutron-camera dir#
elif [[ $HOME == *"jyjohn24"* ]]; then
 	CODE_DIR=$HOME/projects/neutron_camera ##Name of directory containing /asic-neutron-camera dir
elif [[ $HOME == *"rishita"* ]]; then
	CODE_DIR=$HOME/NeutronCamera ##Name of directory containing /asic-neutron-camera dir#
else
	echo 'User not recognised. Exiting' #If you have seen this error, just append your home user area and email to the if statement above
    exit 0
fi


##Hard coded to overwrite default code dir, used in debugging, not needed
CODE_DIR=$HOME/NeutronCamera/updateParams
DIR=/group/tripathilab/projects/neutron_camera/data/ #_DT5751/ ##Name of general /data directory

######################################
#If setting NUM_FILES != "all", then make sure only a single bias and threshold value are specified for a given DATA_DIR, otherwise things get wonky!!
#If you need to run multiple biases and thresholds for a given DATA_DIR, but have NUM_FILES !="all", then submit each combo individually!!
#If NUM_FILES == "all", then feel free to provide multiple biases and thresholds for a given DATA_DIR. This script will submit each combination as a seperate--
#slurm job so each will run in parallel.
######################################

DATA_DIR=EJ276_50mm3_CH19FB_Cf252_Data_3e7samples ##Name of directory containing the relevant data to generate runParams for
BIAS_LIST=(29) #"all", #(29); ##List specific biases (## ## ##) to run over or select "all"
THRESH_LIST=8130 #List the specific thresholds (## ## ##) to run over or select "all"  
CHANNEL="Both" #Select whether to process SOUT, FOUT or both
P=med #State priority to submit the sbatch job with: "high", "med", or "low"
NUM_FILES=10 #How many processed raw data pickle files should be consolidated into one output runParams pickle file? Either "all" or an int value!
TEST_RUN=False #When true, exit the script after a single job has been submitted

if [[ "$BIAS_LIST" == "all" ]]; then
    if [[ "$THRESH_LIST" == "all" ]]; then  
		sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/energy_spectrum.sh $CODE_DIR $DATA_DIR -1 -1 $DIR $P
		if [[ $TEST_RUN == True ]]; then
			exit 0
		fi
	else
		for thresh in "${THRESH_LIST[@]}"; do
			sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/energy_spectrum.sh $CODE_DIR $DATA_DIR -1 $thresh $DIR $P $CHANNEL
			if [[ $TEST_RUN == True ]]; then
							exit 0
			fi
		done
    fi   
else
    if [[ "$THRESH_LIST" == "all" ]]; then
		for bias in "${BIAS_LIST[@]}"; do
	    	sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/energy_spectrum.sh $CODE_DIR $DATA_DIR $bias -1 $DIR $P $CHANNEL
			if [[ $TEST_RUN == True ]]; then
				exit 0
			fi
		done
    else
	    for bias in "${BIAS_LIST[@]}"; do
			for thresh in "${THRESH_LIST[@]}"; do
		 		if [[ "$NUM_FILES" != "all" ]]; then
		 			cd $DIR/pickled_data/$DATA_DIR/
		 			B="Bias_"
		 			B+="$bias"
		 			T="Thresh_"
		 			T+="$thresh"
		 			COUNT=$(ls | grep "$B" | grep "$T" |wc -l)
		 			NUM_RANGES=$(($COUNT / $NUM_FILES))
		 			for i in $(eval echo "{0..$NUM_RANGES}"); do
			 			MIN=$(( i * NUM_FILES + 1 ))
			 			MAX=$(( i * NUM_FILES + NUM_FILES ))
			 			sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/energy_spectrum.sh $CODE_DIR $DATA_DIR $bias $thresh $DIR $P $MIN $MAX $CHANNEL
						if [[ $TEST_RUN == True ]]; then
							exit 0
						fi
		 			done
		 		else
		 	 		sh $CODE_DIR/asic-neutron-camera/Phase1_Tests/slurm_scripts/process_data/energy_spectrum.sh $CODE_DIR $DATA_DIR $bias $thresh $DIR $P $CHANNEL
		 			if [[ $TEST_RUN == True ]]; then
						exit 0
					fi
				 fi
	     	done
	 	done
    fi
fi
