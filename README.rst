NAT & HEPCAT Summer School: Scintillator and SiPMs
=======================
Welcome to the code repository for the NAT & HEPCAT Summer Schools’ Scintillator and SiPMs lab.

Here you can find the for assessing the pulse shape discrimination of scintillators coupled to an onsemi J-series SiPM.


Getting this code
-----------------
Use

.. code:: sh

    git clone https://gitlab.com/BBoxer/sipm_hepcat-nat


Conda Environment
=================
The jupyter notebooks and python code within this repository should use the
`conda environment <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_  provided in the
`yaml file <https://gitlab.com/BBoxer/sipm_hepcat-nat/environment.yml>`_ in the repository.
The conda environment is called **sipm**.

Getting conda
-------------
The instructions below are taken from the
`conda install documentation <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_.
You should also consult the `NERSC python instructions
<https://docs.nersc.gov/development/languages/python/nersc-python/#option-4-install-your-own-python>`_

.. code-block:: sh

    # Go to HOME directory
    cd
    # Get miniconda
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    # Run install script
    bash Miniconda3-latest-Linux-x86_64.sh

Install miniconda in your :code:`$HOME`.

sipm environment
-----------------
To create the **sipm** environment, create it from the environment.yml file provided.
Do this by;

.. code:: sh

    conda env create -f environment.yml


To use the environment;

.. code:: sh

    conda activate sipm


Adding environment to jupyter
-----------------------------
The following lines will allow jupyter to find your conda environment.
The environment will be called **sipm** within jupyter.

.. code:: sh

    # enter the environment
    conda activate sipm
    # add the environment for jupyter
    python -m ipykernel install --user --name sipm --display-name sipm


Repository Overview
-------------------



Contacts
--------

* `Billy Boxer <mailto:bboxer@ucdavis.edu>`_
* `Ben Godfrey <mailto:bpgodfrey@ucdavis.edu>`_
