#!/usr/bin/env python3.9
# File name: arc_utils.py
# Description: Functions used within the processing and analysis of data collected using a single SensL SiPM Cell
# Author: Billy Boxer (bboxer@ucdavis.edu), Jyothis Johnson (jyjohnson@ucdavis.edu) 
# Date: 30-08-21

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import gzip
import csv
import sys
import os
import re
import math
from scipy.signal import find_peaks
import scipy
from scipy.optimize import curve_fit

import glob
import peakutils
import lxml.etree as etree
import textwrap
import io
import time

from pathlib import Path


from pylab import rcParams
rcParams['figure.figsize'] = 10, 8
#Set font for all plots
font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}
matplotlib.rc('font', **font)


##General functions
def str2bool(v):
    """
    Converts a string input to a boolean if it isn't already a boolean

    :param v: the input str or boolean
    :type v: str or bool
    :return: the input boolean or the input string converted to equivalent boolean
    :rtype: boolean
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def gauss(x, amplitude, mean, stdev):
    return amplitude * np.exp( - (x - mean)**2 / (2*stdev ** 2))

def getLastValue(aList):
    """
    Get the last value in a list
    
    :param aList: the input list
    :type aList: list
    :retrun: last entry of list
    :rtype: any
    """
    return aList[-1]


##Get files and load data

def get_files(datapath,testDir):
    """
    Get a list of the csv files for a given data set from arc
    
    :param datapath: path to the directory containg the datasets 
    :type datapath: str
    :param testDir: the name of the directory containing the test data 
    :type testDir: str
    :return: list of file names
    :rtype: list[str]
    """
    
    datapath =datapath+testDir
    dataFiles= [f for f in os.listdir(datapath) if 'csv' in f] #select only csv
    
    return(dataFiles)


def get_data(datapath,dirname,filename):
    """
    Load the relevant aquisition information and pulse data for both SOUT and FOUT
    
    :param datapath: path to the directory containg the datasets 
    :type datapath: str
    :param testDir: the name of the directory containing the test data 
    :type testDir: str
    :param filename: the name of the csv file to be loaded 
    :type testDir: str
    :return: data set containing the time, volt and relevant data aquisition inforamtion for SOUT and FOUT
    :rtype: dataFrame
    """     
    
    
    print("Path: ",datapath)
    print("Directory: ",dirname)
    print("File: ",filename)
    
    #load each column of the csv into a named dataFrame variable
    data=pd.read_csv(datapath+dirname+'/'+filename, sep=',',names=\
                     ["Information_Name","Information_Value","Information_Units","Time_Slow","Volt_Slow","Nan","Information_Name2","Information_Value2","Information_Units2","Time_Fast","Volt_Fast","Nan2"]).reset_index()
 
    #Delete the columns that are not needed
    del data['Information_Name']
    del data['Information_Units']
  
    del data['Information_Name2']
    del data['Information_Units2']
    
    del data['Nan2']
    del data['Nan']
    del data['index']
    
    data.head()
    
    return(data)
    
def get_Noise(df_properties,bin_res=0.125):
#For the Caen data    
#Todo
#Tidy add proper description
    #Initialize Dictionary
    noise_datasets={}

    noise_datasets['SOUT_popt']=[]
    noise_datasets['SOUT_perr']=[]
    noise_datasets['SOUT_bin_heights']=[]
    noise_datasets['SOUT_bin_borders']=[]
    noise_datasets['FOUT_popt']=[]
    noise_datasets['FOUT_perr']=[]
    noise_datasets['FOUT_bin_heights']=[]
    noise_datasets['FOUT_bin_borders']=[]
    noise_datasets['SOUT_baseline']=[]
    noise_datasets['FOUT_baseline']=[]
    noise_datasets['SOUT_Noise_sig']=[]
    noise_datasets['FOUT_Noise_sig']=[]
    noise_datasets['SOUT_Noise_6sig']=[]
    noise_datasets['FOUT_Noise_6sig']=[]
    
    #set the min and max bin based on the minimum and maximum trace information for all events on a given channel
    fbin_max=bin_max=math.ceil(np.max(np.max(df_properties[df_properties['channel']=='FOUT']['trace'])))+1
    fbin_min=math.floor(np.min(np.min(df_properties[df_properties['channel']=='FOUT']['trace'])))-2

    bin_max=bin_max=math.ceil(np.max(np.max(df_properties[df_properties['channel']=='SOUT']['trace'])))+0.5
    bin_min=math.floor(np.min(np.min(df_properties[df_properties['channel']=='SOUT']['trace'])))-0.5

    bin_num= math.ceil(abs(bin_max-bin_min) / bin_res)
    fbin_num= math.ceil(abs(fbin_max-fbin_min) / bin_res)
   
    
    
    for e in df_properties['event'].unique():
        event_traces=(df_properties[df_properties['event']==e]).reset_index()

        #Calculate noise parameters for the given file
        sguess_mean= np.average(event_traces['trace'][0])
        sguess_sig= np.sqrt(np.mean(np.array(event_traces['trace'][0]-sguess_mean)**2))
        sbin_heights, sbin_borders= np.histogram(event_traces['trace'][0], bins=bin_num)
        noise_datasets['SOUT_bin_heights'].append(sbin_heights)
        noise_datasets['SOUT_bin_borders'].append(sbin_borders)
        sbin_centers = sbin_borders[:-1] + np.diff(sbin_borders) / 2
        sbound_vals=((0.1*np.max(sbin_heights),-np.inf, -np.inf), (1.9*np.max(sbin_heights), np.inf, np.inf))
        spopt, spcov = curve_fit(gauss, sbin_centers, sbin_heights, p0=[np.max(sbin_heights), sguess_mean, sguess_sig],maxfev=50000, bounds=sbound_vals)
        
        try:
            fguess_mean= np.average(event_traces['trace'][1])
            fguess_sig= np.sqrt(np.mean(np.array(event_traces['trace'][1]-fguess_mean)**2))
            fbin_heights, fbin_borders= np.histogram(event_traces['trace'][1], bins=fbin_num if fbin_num>0 else bin_num)
            noise_datasets['FOUT_bin_heights'].append(fbin_heights)
            noise_datasets['FOUT_bin_borders'].append(fbin_borders)
            fbin_centers = fbin_borders[:-1] + np.diff(fbin_borders) / 2
            fbound_vals=((0.1*np.max(fbin_heights),-np.inf, -np.inf), (1.9*np.max(fbin_heights), np.inf, np.inf))
            fpopt, fpcov = curve_fit(gauss, fbin_centers, fbin_heights, p0=[np.max(fbin_heights), fguess_mean, fguess_sig], bounds=fbound_vals)
        
            noise_datasets['FOUT_baseline'].append(fpopt[1])
            noise_datasets['FOUT_Noise_6sig'].append(np.abs(fpopt[2])*3)  
            noise_datasets['FOUT_Noise_sig'].append(np.abs(fpopt[2]))
            noise_datasets['FOUT_popt'].append(fpopt)
            noise_datasets['FOUT_perr'].append(np.sqrt(np.diag(fpcov)))

        except:
           # print('Fout noise data failed')
            noise_datasets['FOUT_baseline'].append(float('Nan'))
            noise_datasets['FOUT_Noise_6sig'].append(float('Nan'))  
            noise_datasets['FOUT_Noise_sig'].append(float('Nan'))
            noise_datasets['FOUT_popt'].append(float('Nan'))
            noise_datasets['FOUT_perr'].append(float('Nan'))
            
        noise_datasets['SOUT_baseline'].append(spopt[1])
        noise_datasets['SOUT_Noise_6sig'].append(np.abs(spopt[2])*3)
        noise_datasets['SOUT_Noise_sig'].append(np.abs(spopt[2]))
        noise_datasets['SOUT_popt'].append(spopt)
        noise_datasets['SOUT_perr'].append(np.sqrt(np.diag(spcov)))
    return(noise_datasets)

##convertfor adcc to mV
def adcc_to_mV(adcc, bitRes):
    """
    Convert from adcc to mV, 14 bit resolution
    
    :param adcc: trace value in adcc 
    :type adcc: float
    :return: trace value in mV
    :rtype: float

    mm it should be x1 not x2 for new digitzer and the minus part is more complicated
    15:54
    It needs to be -(0.5-reference)
    15:54
    Where reference is the voltage converted 16bit DC offset value
    15:55
    That is slightly different for fout and sout for the new digitzer

    """
    return(((int(adcc)/(2**int(bitRes)))*2-1)*1000)

# #Information in filename
# def get_acqInfo(filename,directory,Threshold=False):
#     """
#     Get the aquisition information for the xml file being loaded
    
#     :param filename: full path and filename of xml file 
#     :type datapath: str
#     :param Threshold: select whether or not to extract the cean trigger threshold value 
#     :type testDir: boolean
#     :return: SiPM bias, length of data in samples, cean trigger threshold
#     :rtype: float, float, float
#     """
#     if "CAEN" in filename:
#         return(float('Nan'),float('Nan'))
#     else:
#         splitDir = re.split("_",re.split(directory,filename)[1])
#         print(splitDir)
#         bias = float(re.split("V",splitDir[splitDir.index('Bias')+1])[0].replace("p", "."))
#         sampleLength = re.split("us",splitDir[9])[0]    
#         bitRes = (re.split("bit",splitDir[-1])[0])
#         thresh = adcc_to_mV(int(splitDir[splitDir.index('Thresh')+1]), int(bitRes)) if Threshold else float('Nan')
#         print(bias, sampleLength, bitRes, thresh)
#     return(bias,sampleLength,thresh, bitRes)

def get_acqInfo(filename,directory,Threshold=False):
    """
    Get the aquisition information for the xml file being loaded
    
    :param filename: full path and filename of xml file 
    :type datapath: str
    :param Threshold: select whether or not to extract the cean trigger threshold value 
    :type testDir: boolean
    :return: SiPM bias, length of data in samples, cean trigger threshold
    :rtype: float, float, float
    """
    if "CAEN" in filename:
        return(float('Nan'),float('Nan'))
    else:
        splitDir = re.split("_",re.split(directory,filename)[1])
        bias=float(re.split("V",splitDir[splitDir.index('Bias')+1])[0].replace("p", "."))
        sampleLength = re.split("us",splitDir[3])[0]    
        thresh=(adcc_to_mV(float(splitDir[splitDir.index('Thresh')+1]), 14)) if Threshold else float('Nan')
        
    return(bias,sampleLength,thresh)



#read pki files
def pickle_reader(filename):
    """
    Load .pki file and get the aquisition information 
    
    :param filename: name of pki file 
    :type filename: str
    :return: Frequency samples taken at, bit resolution of data, events DataFrame['event','channel','trace'] 
    :rtype: str, str, DataFrame[str,str lst[float]]
    """
    print('FILE    ',filename)
    df_events = pd.read_pickle(filename)
    print('loaded')
    sampleFreq = re.split('_',re.split('Hz',filename)[0])[-1]
    sampleRes = re.split('_',re.split('bit',filename)[0])[-1]
    return(sampleFreq,sampleRes,df_events)

#File convert: convert to pki file
def convert_to_pki(directory,dir_data,output_dir, file=False, filenames=None):
    """
    convert the file to a pki 
    :param directory: name of directory containing data directories
    :type directory: str
    :param dir_data: name of directory containing data files
    :type dir_data: str
    :return: None 
    :rtype: NA
    """
    print('{}xml_files/{}'.format(directory,dir_data))
    #Make directory (and parents) for pickle output  
    Path(output_dir).mkdir(parents=True, exist_ok=True) 
    
    if not file:
        #glob all ".xml" into a lift of filenames
        filenames=(glob.glob(directory+'xml_files/'+dir_data+'/*.xml'))
    
    for testDir in filenames:
        #Make an output file name based on the scintillator and parameters in xml file name
        output_filename = re.split('.xml',re.split('/',testDir)[-1])[0]
        f=Path('{}/{}_256000000Hz_14bit.pki'.format(output_dir,output_filename))
        g=Path('{}/{}_250000000Hz_14bit.pki'.format(output_dir,output_filename))
        if f.exists() or g.exists():
            print("\nFile: {f} already exists. Moving to next file.".format(f=testDir), flush=True)
            continue

        print("\nStart of processing: {}".format(testDir),flush=True)

        try:
            bias,sampleLength,thresh,sampleFreq, sampleRes, df_properties = load_Data(testDir,directory)
            print(df_properties.head(), flush=True)

        except:
            print("Failed to get properties for: ", testDir, flush=True)
            continue
        
        #Save the data frame as a .pki for easier loading later
        df_properties.to_pickle('{}/{}_{:.0f}Hz_{:.0f}bit.pki'.format(output_dir,output_filename, sampleFreq, sampleRes))    
        


#File convert: convert to pki file
def convert_to_pki_parallel(directory,dir_data,output_dir,filenames):
    """
    convert the file to a pki using parallel processing
    :param directory: name of directory containing data directories
    :type directory: str
    :param dir_data: name of directory containing data files
    :type dir_data: str
    :param output_dir: directory to store the output .pki file
    :type output_dir: str
    :return: None 
    :rtype: NA
    """
    print('{}xml_files/{}'.format(directory,dir_data))
    #Make directory (and parents) for pickle output  
    Path(output_dir).mkdir(parents=True, exist_ok=True) 
    
    testDir=filenames
    #Make an output file name based on the scintillator and parameters in xml file name
    output_filename = re.split('.xml',re.split('/',testDir)[-1])[0]
    f=Path('{}/{}_256000000Hz_14bit.pki'.format(output_dir,output_filename))
    g=Path('{}/{}_250000000Hz_14bit.pki'.format(output_dir,output_filename))
    
    if f.exists() or g.exists():
        print("\nFile: {f} already exists. Moving to next file.".format(f=testDir), flush=True)
        exit()
    
    print("\nStart of processing: {}".format(testDir),flush=True)
        
    #try:
    bias,sampleLength,thresh,sampleFreq, sampleRes, df_properties = load_Data(testDir,directory)
    print(df_properties.head(), flush=True)
        
    #except:
    #    print("Failed to get properties for: ", testDir, flush=True)
    #   exit()
    #Save the data frame as a .pki for easier loading later
    df_properties.to_pickle('{}/{}_{:.0f}Hz_{:.0f}bit.pki'.format(output_dir,output_filename, sampleFreq, sampleRes))    



        
##File reader: load xml or pki files
def load_Data(filename,directory):
    """
    Load the data, calls pickle_reader (pki) fast_itter(xml0 or hdf5 reader() 
    :param filename: name of data file 
    :type filename: str
    :param directory: name of directory containing data directories
    :type directory: str
    :return: SiPM Bias, Aquisition length ,SiPM Threshold, Frequency samples taken at, bit resolution of data, events DataFrame['event','channel','trace'] 
    :rtype: str, str, str,float,str DataFrame[float,float lst[float]]
    """
    #Get the aquisition information from the filename
    try:
        bias,sampleLength,thresh = get_acqInfo(filename,directory,True)
        print("Found acq info", flush=True)#, flush=True)
    except:
        print("Failed to get aquisition information for: ", filename, flush=True)#, flush=True)
        
    #Get the event information
    try:
        if '.pki' in filename:
            print("loading .pki file...", flush=True)
            sampleFreq, sampleRes, df_properties = pickle_reader(str(filename))
        elif '.xml' in filename:
            context = etree.iterparse( filename, tag='digitizer' )
            sampleFreq, sampleRes = fast_iter(context, tag='digitizer')
            context = etree.iterparse( filename, tag='event' )
            df_properties = fast_iter(context, tag='event')
        # elif '.h5' in filename:
        #     sampleFreq, sampleRes, df_properties = hdf5_reader(filename)

        print(df_properties.head(), flush=True)
    
    except:
        print("Failed to get event properties for: ", filename, flush=True)
    
    return(bias,sampleLength,thresh,float(sampleFreq), sampleRes, df_properties)

##File reader: xml files
def fast_iter(context,tag):
    """
    Faster iter method for loading xml files
    
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm

    :param context: optional file which contains a <Context> tag
    :type context: xml context
    :param tag: name of tag 
    :type tag: str
    :return: events DataFrame['event','channel','trace'] 
    :rtype: DataFrame[float,float lst[float]]
    
    """
    sample_resolution = 14
    df_properties = pd.DataFrame()
    for event, elem in context:
        if elem.tag=='digitizer':      
            sample_frequency=float([subelem.attrib.get('hz') for subelem in elem if subelem.attrib.get('hz') != None][-1])
            sample_resolution=float([subelem.attrib.get('bits') for subelem in elem if subelem.attrib.get('bits') != None][-1])                
        #If the element is the event proceed
        if elem.tag=='event':
        #    print("Event ID: ",elem.get('id'))
            #Loop over each subelement in the "event" element
            for subelem in elem:
                #If the subelement is the trace continue
                if subelem.tag=='trace':
                #    print("Channel:" , subelem.attrib.get('channel'))
                    #Take the values of the trace and convert the space seperated string into a list of ints and append to a new list
                    trace_in=(subelem.text)
                    trace_reduce=([adcc_to_mV(int(i), sample_resolution) for i in trace_in.split()])

                    #Get the event ID and whether or not the signal is SOUT or FOUT
                    eventID=float(elem.get('id'))
                    channelID=('SOUT' if subelem.attrib.get('channel')=="0" else "FOUT")

                    ##Populate a dataframe with these values
                    df_temp=pd.DataFrame([[eventID,channelID,trace_reduce]],columns=['event','channel','trace'],index=None)
                    df_properties=df_properties.append(df_temp)
        
        
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context
    #Reset the index and remove the redundant column                
    df_properties=(df_properties.reset_index())
    del df_properties['index']

    if tag=='digitizer':
        return(sample_frequency,sample_resolution)
    else:
        return (df_properties)
       

def get_peakInfo(dfToFindPeaks,p_promY,p_height,p_dist):
    """
    Get peak information using scipy peakfinder 
    
    :param dfToFindPeaks: DataFrame containing the event trace 
    :type dfToFindPeaks: DataFrame
    :param promY: Prominence above baseline to accept as peak 
    :type promY: flaot
    :param dist: minimum seperation in samples allowed between possible peaks 
    :type dist: float
    :return: the sample point of the peaks, the width at 90% peak height, the width at 50% peak height
    :rtype: list[float],list[float],list[float]
    """
    peaks,_ = scipy.signal.find_peaks(dfToFindPeaks,prominence=p_promY,distance=p_dist,height=p_height)  
    width90 = scipy.signal.peak_widths(dfToFindPeaks, peaks,rel_height=0.9)
    widthHalf = scipy.signal.peak_widths(dfToFindPeaks, peaks,rel_height=0.5)   
    
    return(peaks,width90,widthHalf)


def get_gain(supplyVolt):
    """   
    Determine the gain of the SiPM based on the supply voltage
    Slope and intercept extracted from https://sensl.com/downloads/ds/DS-MicroJseries.pdf 
    :param supplyVolt: SiPM supply voltage
    :type supplyVolt: float
    :return: gain
    :rtype: float

    """
    overvolt  = supplyVolt - 24.7  ## range from 24.2 to 24.7
    slope=1012049
    intercept = 355084

    return(slope*overvolt+intercept)

def phd_conversion(supplyVolt, board='FB'):
    """   
    Calculates the conversion factor to divide integrated area in mA*s by to return photons detected (phd)
    :param supplyVolt: SiPM supply voltage
    :type supplyVolt: float
    :return: conversion
    :rtype: float

    """
    eCharge = 1.602e-19
    resistor = 50 if board == 'FB' else 10
    if board == 'Done':
        resistor =1
    conversion = resistor * eCharge * get_gain(supplyVolt) *1e3 # 1e3 to account for data being in mA*s
    
    return conversion

def generate_filelist(bias, thresh, scint, source, directory, scintSize='6mm3', numSamples= '1e6samples', num_to_process=-1, multiple=False):
    """   
    Generates a list of filenames that meet all given criteria for a given full path directory input
    :param bias: SiPM supply voltage to cut down filenames list with
    :type supplyVolt: int
    param thresh: Digitzer threshold to cut down filenames list with
    :type thresh: int
    param scint: Type of scintillator to cut down filenames list with
    :type scint: str
    param source: Type of radioactive source to cut down filenames list with
    :type source: str
    param directory: Inital directory containing runParam output files
    :type directory: str
    param num_to_process: For runParam outputs spread out over many files, how many to process
    :type num_to_process: str
    param multiple: Flag that runParam outputs are spread out over many files
    :type multiple: bool
    :return: filenames, index_max
    :rtype: list, int

    """
   
    filenames = (glob.glob(directory+"{}_{}_CH19FB_{}_Data_{}/*".format(scint, scintSize, source, numSamples)))
    
    #Cut to selected numberSamples parameter:
    filenames=[ x for x in filenames if numSamples in x ]
    
    #Cut to selected Scintillator and Source types:
    filenames=[ x for x in filenames if (scint in x and source in x) ]
    #print('Filenames with scint and source cuts: {}'.format(filenames), flush=True)
    
    #Cut the list to selected bias
    if bias != -1:
        filenames=[ x for x in filenames if 'Bias_'+str(bias) in x ]
        #print('Filenames with bias cuts: {}'.format(filenames), flush=True)
    
    #Cut the list to selected threshold
    if thresh != -1:
        filenames=[ x for x in filenames if 'Thresh_'+str(thresh) in x ]
        #print('Filenames with threshold cuts: {}'.format(filenames), flush=True)
    
    #Cut the list to selected index range
    if multiple:
        if num_to_process != -1:
            filenames_index=[]
            count=0
            while count < num_to_process:
                cmin= count*10 +1
                cmax= count*10 +10
                for x in filenames:
                    if 'Index_{}_{}'.format(cmin,cmax) in x:
                        filenames_index.append(x)
                count+=1
            filenames=filenames_index
        else:
            cmax='all'
    else:
        cmax='all'
    
    index_max=cmax
    return filenames, index_max

def get_dfParams_key(filenames, key, int_length=250):
    """   
    For a given list of filenames (assumed to be multiple parts of the runParams output),
    and a given key value, it will loop through all of the files and return a combined numpy array/ndarray
    with the values of the given key for all files.
    
    :param filenames: input list of filenames to run over 
    :type filenames: list
    param key: the value of this key for every event in every file will be appended to comb_list
    :type key: str
    param int_length: if trying to append integration output, define the length of time for integration,
                        or supply a list of various ints for multiple integration outputs
    :type int_length: int or list of ints
    :return: comb_list, num_failed
    :rtype: array or ndarray, int

    """
    comb_list=[]
    num_failed=0
    for f in filenames:
        df_runParams=pd.read_pickle(f)
        
        for k in range(len(df_runParams[key])):
            try:
                if (key == 'SOUT area_mVs' or key == 'SOUT area_mA_ns' or key == 'FOUT area_mVs') and (int_length != 'all'):
                    if type(int_length) == int:
                        comb_list.append(df_runParams[key][k][int_length])
                    elif type(int_length) == list:
                        for i in int_length:
                            tmp=[]
                            try:
                                tmp.append(df_runParams[key][k][i])
                            except:
                                tmp.append(-999)
                        comb_list.append(tmp)
                else:
                    comb_list.append(df_runParams[key][k])
            except:
                num_failed+=1
                #comb_list.append(-999)
                pass
    print("Number of Events failed in generating combined array: {}".format(num_failed), flush=True)
    comb_list=np.array(comb_list)
        
    return comb_list, num_failed


def adjust_PDE(supplyVolt,detEfficiency):
    """
    Adjust the PDE based on the supply voltage to the SiPM
    Slope and intercept extracted from https://sensl.com/downloads/ds/DS-MicroJseries.pdf
    
    :param supplyVolt: SiPM supply voltage 
    :type supplyVolt: float
    :param detEfficiency: the non adjusted PDE 
    :type detEfficiency: float
    :return: adjusted PDE
    :rtype: float
    """     
    
    overvolt  = supplyVolt - 24.7  ## range from 24.2 to 24.7
    slope=0.06902051144714151
    intercept = 0.5858397458416008

    return((slope*overvolt+intercept)*detEfficiency)

class fixedValues:
    '''
    Class to store fixed values that are used in mutlplie paces
    '''

    
    def __init__(self,sampleRate = 1/256000000,breakVolt = 24.7, sources = ['Co57', 'Cd109', 'Cs137', 'AmBe', 'Cf252'], phd_to_keV_SB=[2.31,0], phd_to_keV_EJ=[1.62,0]):

    
        """
        :param sampleRate: the sample rate of the caen digitiser
        :type sampleRate: float
        :para breakVolt: breakdown voltage of the SiPM
        :type breakVolt: float
        :para sources: list of the radioactive sources used
        :type sources: list[str]
        """     
        self.sampleRate = sampleRate
        self.breakVolt = breakVolt
        self.sources = sources
        self.phd_to_keV_SB = phd_to_keV_SB
        self.phd_to_keV_EJ = phd_to_keV_EJ