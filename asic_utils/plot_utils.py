#!/usr/bin/env python3.9
# File name: arc_utils.py
# Description: Functions used within the processing and analysis of data collected using a single SensL SiPM Cell
# Author: Billy Boxer (bboxer@ucdavis.edu), Jyothis Johnson (jyjohnson@ucdavis.edu) 
# Date: 30-08-21

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.markers as mmarker
import numpy as np
import pandas as pd
import gzip
import csv
import sys
import os
import re
import math
from scipy.signal import find_peaks
import scipy
from scipy.optimize import curve_fit

import glob
import peakutils
import lxml.etree as etree
import textwrap
import io
import time

from pathlib import Path
from seaborn import heatmap


from pylab import rcParams
rcParams['figure.figsize'] = 10, 8
#Set font for all plots
font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}
matplotlib.rc('font', **font)

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')



def make_plot_signalVsample(data,channel,testDir,filename,peaks,width90,widthHalf,zoom=True,title=True):
    ns=1e-9
    yToPlot=data['Volt_Slow'] if channel =='Slow' else data['Volt_Fast']
    xToPlot=data['Time_Slow'] if channel =='Slow' else data['Time_Fast']
    plotLabel=filename+': SOUT' if channel =='Slow' else filename+': FOUT'
    plt.plot(xToPlot/ns,yToPlot,label=plotLabel)
    
    peakColor='red' if channel == 'Slow' else 'green'
    plt.scatter(xToPlot[peaks[:]]/ns,yToPlot[peaks[:]],color=peakColor,marker='x',label='{} channel peaks {}'.format(channel,len(peaks)),s=220)
    
    pNum=len(peaks)
    if pNum==1 and zoom==True:
             plt.xlim(xToPlot[peaks[-1]-100]/ns,xToPlot[peaks[-1]+100]/ns)
    plt.title('SiPM Data 6_11_2021 - {}'.format(testDir)) if title==True else None
    plt.ylabel('Volts [V]')
    plt.xlabel('Time [ns]')
    plt.legend(fontsize=14)

def make_plot_whole_file(data,channel,testDir,filename):
    xToPlot=data['Volt_Slow'] if channel =='Slow' else data['Volt_Fast']
    yToPlot=data['Time_Slow'] if channel =='Slow' else data['Time_Fast']
    plotLabel=filename+': SOUT' if channel =='Slow' else filename+': FOUT'
    plt.plot(yToPlot,xToPlot,label=plotLabel)
    
    plt.title('SiPM Data 6_11_2021 - {}'.format(testDir))
    plt.ylabel('Volts [V]')
    plt.xlabel('Time [s]')
    plt.legend(fontsize=14)    
    

#general plotting class
class general_plotter:
    '''
    This class provides a consistent but tunable default plotting interface. It also provides an easy method to plot various data from files to 
    different plots by calling different instances of this class.
    '''

    
    def __init__(self, set_size_inches=(20,10), set_dpi=100, facecolor='white', 
                  nrow=1, ncol=1, fig_suptitle=None, fig_suptitle_size=24):
        
        """
        Initialize a instance of plt.subplots with a specified number of rows 
        and columns and provided super title.
        
        :param set_size_inches: set the figure size in inches
        :type set_size_inches: list
        :param set_dpi: set the resolution of generated figure
        :type testDir: int
        :param facecolor: selects the background color for figure
        :type facecolor: str
        :param nrow: used to select the number of rows for the figure (rows*cols equals total subplots)
        :type nrow: int
        :param ncol: used to select the number of columns for the figure (rows*cols equals total subplots)
        :type ncol: int
        :param fig_suptitle: The super title common to all subplots in the figure
        :type fig_suptitle: str
        :param fig_suptitle_size: The size of the super title common to all subplots in the figure
        :type fig_suptitle_size: int
        """
        
        self.set_size_inches = set_size_inches
        self.set_dpi = set_dpi
        self.facecolor = facecolor
        self.nrow = nrow
        self.ncol = ncol
        self.fig_suptitle = fig_suptitle
        self.tot_subplots = self.nrow*self.ncol
    
        self.fig, self.ax = plt.subplots(self.nrow,self.ncol, squeeze=False)
        self.fig.set_size_inches(self.set_size_inches[0],self.set_size_inches[1])
        self.fig.set_dpi(self.set_dpi)
        self.fig.set_facecolor(self.facecolor)
        
        if type(self.fig_suptitle) == str:
            self.fig.suptitle(self.fig_suptitle, fontname='DejaVu Sans', fontsize=fig_suptitle_size, fontweight = "bold")
    
    def add_subplot_plot(self, index_row, index_col, ydata=None, xdata=None, color_data=None, plotType=None, 
                         plt_label=None, linewidth=2, linestyle=None, s_size=None, numBins=None, binRange=None, log=True,
                         histtype='step', alpha=0.7, color=None, marker=None, cmap=None, cbar_orient=None, cbar_label=None, clim=None, shading='auto',
                         gen_mesh=True, fill = False, sb_fmt=None, sb_invert_y=False, annotate=True, norm=matplotlib.colors.Normalize()):
        
        
        """
        In order to provide the ability to add data to each subplot independently and to allow data to be plotted to different open figures,
        an add_subplot_plot function was included. When initializing a class instance, you specify the number of nrows and ncols. This then
        allows for direct addressing of a specific row and column of the subplot matrix and adding data to plot.
        
        :param index_row: what row of initialized figure's subplots to address (for a one subplot figure, use 0)
        :type index_row: int
        :param index_col: what col of initialized figure's subplots to address (for a one subplot figure, use 0)
        :type index_col: int
        :param ydata: ydata to add to addressed subplot
        :type ydata: array
        :param xdata: xdata to add to addressed subplot
        :type xdata: array
        :param plotType: select the type of plot to generate; either plot, scatter, or hist for the moment.
        :type plotType: str
        :param plt_label: label for added ydata for addressed subplot of figure
        :type plt_label: str
        :param linewidth: The thickness of the line for data added to addressed subplot of figure for plotType: plot
        :type linewidth: int
        :param linestyle: set the style of the line "solid", "dotted", "dashed" or "dashdot"
        :type linestyle:str
        :param s_size: The thickness of each pt for data added to addressed subplot of figure for plotType: scatter
        :type s_size: int
        :param numBins: The number of bins for histogram of data added to addressed subplot of figure for plotType: hist
        :type numBins: int
        :param binRange: The start and end bin value for histogram of data added to addesseed subplot of figure for plotType: hist
        :type: tuple of floats
        :param log: Specifies whether the histogram should have y-axis in log scale for addressed subplot of figure for plotType: hist
        :type log: bool
        :param histtype: defines whether histogram bins should be filled in or not. 
        :type histtype: str
        :param color: the colour of the plotting object, must be in  matplotlib.colors
        :type color: str
        :param marker: the marker style for scatter plots from matplotlib.markers
        :type marker: string
        :param norm: a normalizations used to scale luminance data to [0, 1]
        :type: norm: instance of matplotlib.colors
        """
        
        if plotType == 'plot':
            if color not in mcolors.cnames and color not in mcolors.BASE_COLORS:
                color = 'blue'        
            if type(plt_label) == str:
                if type(xdata) == type(None):
                    self.ax[index_row,index_col].plot(ydata,label=plt_label,color=color,
                                                  linewidth=linewidth, alpha=alpha)
                else:
                    self.ax[index_row,index_col].plot(xdata, ydata,label=plt_label,color=color,
                                                  linewidth=linewidth, alpha=alpha)
            elif plt_label == None:
                if type(xdata) == type(None):
                    self.ax[index_row,index_col].plot(ydata, color=color, linewidth=linewidth, alpha=alpha)
                else:
                    self.ax[index_row,index_col].plot(xdata, ydata, color=color, linewidth=linewidth, alpha=alpha)
        
        elif plotType == 'scatter':
            if color not in mcolors.cnames and color not in mcolors.BASE_COLORS:
                color = 'black'        

            if marker not in  mmarker.MarkerStyle.markers:
                marker = 'o'
                
            if type(plt_label) == str: 
                if (type(s_size) == float or type(s_size) == int):
                    self.ax[index_row,index_col].scatter(x=xdata, y=ydata, s=s_size, color=color, marker=marker, label=plt_label, alpha=alpha)
                else:
                    self.ax[index_row,index_col].scatter(x=xdata, y=ydata, color=color, marker=marker, label=plt_label, alpha=alpha)
            else:
                if (type(s_size) == float or type(s_size) == int):
                    self.ax[index_row,index_col].scatter(x=xdata, y=ydata, s=s_size, color=color, marker=marker, alpha=alpha)
                else:
                    self.ax[index_row,index_col].scatter(x=xdata, y=ydata, color=color, marker=marker, alpha=alpha)
        
        elif plotType == 'hist':
            if type(plt_label) == str: 
                if type(numBins) == int:
                    self.ax[index_row,index_col].hist(ydata, bins=numBins, range=binRange, log=log, 
                                                  histtype=histtype, label=plt_label, alpha=alpha)
                else:
                    self.ax[index_row,index_col].hist(ydata, log=log, range=binRange, 
                                                  histtype=histtype, label=plt_label, alpha=alpha)
            else:
                if type(numBins) == int:
                    self.ax[index_row,index_col].hist(ydata, log=log, bins=numBins, range=binRange, 
                                                  histtype=histtype, alpha=alpha)
                else:
                    self.ax[index_row,index_col].hist(ydata, log=log, range=binRange,
                                                  histtype=histtype, alpha=alpha)
        
        elif plotType == 'colormap_scatter':
            if type(color_data) != list: 
                if type(color_data) != np.ndarray:
                    assert color_data != None, 'If colormap_scatter is selected, please provide color_data of equal length to x and y data...'
            
            if cmap == None:
                cmap = 'viridis'

            if type(plt_label) == str:
                if (type(s_size) == float or type(s_size) == int):
                    pcm=self.ax[index_row,index_col].scatter(x=xdata, y=ydata, c=color_data,  s=s_size, label=plt_label, alpha=alpha, cmap=cmap)
                else:
                    pcm=self.ax[index_row,index_col].scatter(x=xdata, y=ydata, c=color_data, label=plt_label, alpha=alpha, cmap=cmap)
            else:
                if (type(s_size) == float or type(s_size) == int):
                    pcm=self.ax[index_row,index_col].scatter(x=xdata, y=ydata, c=color_data,  s=s_size, alpha=alpha, cmap=cmap)
                else:
                    pcm=self.ax[index_row,index_col].scatter(x=xdata, y=ydata, c=color_data, alpha=alpha, cmap=cmap)
            
            if type(cbar_label) == str:
                if type(cbar_orient) == str:
                    self.fig.colorbar(pcm, ax=self.ax[index_row,index_col], label=cbar_label, orientation=cbar_orient)
                else:
                    self.fig.colorbar(pcm, ax=self.ax[index_row,index_col], label=cbar_label)
            else:
                if type(cbar_orient) == str:
                    self.fig.colorbar(pcm, ax=self.ax[index_row,index_col], orientation=cbar_orient)
                else:
                    self.fig.colorbar(pcm, ax=self.ax[index_row,index_col])
        
        elif plotType == 'colormap_quadmesh':
            if type(color_data) != list: 
                if type(color_data) != np.ndarray:
                    assert color_data != None, 'If colormap_quadmesh is selected, please provide color_data of shape: (len(x),len(y))...'

            if cmap == None:
                cmap = 'viridis'
            
            if gen_mesh:
                xdata , ydata = np.meshgrid(xdata, ydata)

            if clim != None:
                quadmesh = self.ax[index_row,index_col].pcolormesh(xdata, ydata, color_data, vmin=clim[0], vmax=clim[1], cmap=cmap, shading=shading)
            else:
                quadmesh = self.ax[index_row,index_col].pcolormesh(xdata, ydata, color_data, cmap=cmap, shading=shading)
            
            if type(cbar_label) == str:
                if type(cbar_orient) == str:
                    self.fig.colorbar(quadmesh, ax=self.ax[index_row,index_col], label=cbar_label, orientation=cbar_orient)
                else:
                    self.fig.colorbar(quadmesh, ax=self.ax[index_row,index_col], label=cbar_label)
            else:
                if type(cbar_orient) == str:
                    self.fig.colorbar(quadmesh, ax=self.ax[index_row,index_col], orientation=cbar_orient)
                else:
                    self.fig.colorbar(quadmesh, ax=self.ax[index_row,index_col])
            
        elif plotType == 'hist2d':
            if cmap == None:
                cmap = 'viridis'
            assert (numBins != None), 'Please provide an int value that will be used for both x and y or [int,int] if different numbers of bins are required in x and y or provide one (two) array(s) for the two dimensions that will be interpreted as bin edges...'
            if binRange != None:
                if clim != None:
                    hist2d=self.ax[index_row,index_col].hist2d(xdata, ydata, bins=numBins, range=binRange, cmin=clim[0], cmax=clim[1], cmap=cmap, shading=shading, norm=norm)
                else:
                    hist2d=self.ax[index_row,index_col].hist2d(xdata, ydata, bins=numBins, range=binRange, cmap=cmap, shading=shading, norm=norm)
            else:
                if clim != None:
                    hist2d=self.ax[index_row,index_col].hist2d(xdata, ydata, bins=numBins, cmin=clim[0], cmax=clim[1], cmap=cmap, shading=shading, norm=norm)
                else:
                    hist2d=self.ax[index_row,index_col].hist2d(xdata, ydata, bins=numBins, cmap=cmap, shading=shading, norm=norm)
            
            if type(cbar_label) == str:
                if type(cbar_orient) == str:
                    self.fig.colorbar(hist2d[3], ax=self.ax[index_row,index_col], label=cbar_label, orientation=cbar_orient)
                else:
                    self.fig.colorbar(hist2d[3], ax=self.ax[index_row,index_col], label=cbar_label)
            else:
                if type(cbar_orient) == str:
                    self.fig.colorbar(hist2d[3], ax=self.ax[index_row,index_col], orientation=cbar_orient)
                else:
                    self.fig.colorbar(hist2d[3], ax=self.ax[index_row,index_col])

        elif plotType == 'axhline':
            if color not in mcolors.cnames and color not in mcolors.BASE_COLORS:
                        color = 'black'        
            if linestyle not in ["solid", "dotted", "dashed", "dashdot"]:
                linestyle = 'dashed'
            if type(plt_label) == str: 
                if (type(s_size) == float or type(s_size) == int):
                        
                    self.ax[index_row,index_col].axhline(y=ydata, lw=s_size, c=color, linestyle=linestyle, label=plt_label, alpha=alpha)
                else:
                    self.ax[index_row,index_col].axhline(y=ydata, c=color, linestyle=linestyle, label=plt_label, alpha=alpha)
            else:
                if (type(s_size) == float or type(s_size) == int):
                    self.ax[index_row,index_col].axhline(y=ydata, lw=s_size, c=color, linestyle=linestyle, alpha=alpha)
                else:
                    self.ax[index_row,index_col].axhline(y=ydata, c=color, linestyle=linestyle, alpha=alpha)

        elif plotType == 'axvline':
            if color not in mcolors.cnames and color not in mcolors.BASE_COLORS:
                        color = 'black'        
            if linestyle not in ["solid", "dotted", "dashed", "dashdot"]:
                linestyle = 'dashed'
            if type(plt_label) == str: 
                if (type(s_size) == float or type(s_size) == int):
                        
                    self.ax[index_row,index_col].axvline(xdata, lw=s_size, c=color, linestyle=linestyle, label=plt_label, alpha=alpha)
                else:
                    self.ax[index_row,index_col].axvline(xdata, c=color, linestyle=linestyle, label=plt_label, alpha=alpha)
            else:
                if (type(s_size) == float or type(s_size) == int):
                    self.ax[index_row,index_col].axvline(xdata, lw=s_size, c=color, linestyle=linestyle, alpha=alpha)
                else:
                    self.ax[index_row,index_col].axvline(xdata, c=color, linestyle=linestyle, alpha=alpha)

        elif plotType == 'stairs':
            if type(plt_label) == str: 
                if type(color) == str:
                    self.ax[index_row,index_col].stairs(values=ydata, edges=xdata, color=color, label=plt_label, alpha=alpha, fill=fill)                         
                else:
                    self.ax[index_row,index_col].stairs(values=ydata, edges=xdata, label=plt_label, alpha=alpha, fill=fill)
            else:
                if type(color) == str:
                    self.ax[index_row,index_col].stairs(values=ydata, edges=xdata, color=color, alpha=alpha, fill=fill)
                else:
                    self.ax[index_row,index_col].stairs(values=ydata, edges=xdata, alpha=alpha, fill=fill)
        
        elif plotType == 'sb_heatmap':
            if cmap == None:
                cmap = 'viridis'
            
            if type(cbar_label) == str:
                if type(cbar_orient) == str:
                    cbar_kws={'label':cbar_label,'orientation':cbar_orient}
                    self.ax[index_row,index_col]=heatmap(data=xdata, annot=annotate, fmt=sb_fmt, cmap=cmap, cbar_kws=cbar_kws, ax = self.ax[index_row,index_col])
                else:
                    cbar_kws={'label':cbar_label}
                    self.ax[index_row,index_col]=heatmap(data=xdata, annot=annotate, fmt=sb_fmt, cmap=cmap, cbar_kws=cbar_kws, ax = self.ax[index_row,index_col])
            else:
                if type(cbar_orient) == str:
                    cbar_kws={'orientation':cbar_orient}
                    self.ax[index_row,index_col]=heatmap(data=xdata, annot=annotate, fmt=sb_fmt, cmap=cmap, cbar_kws=cbar_kws, ax = self.ax[index_row,index_col])
                else:
                    self.ax[index_row,index_col]=heatmap(data=xdata, annot=annotate, fmt=sb_fmt, cmap=cmap, ax = self.ax[index_row,index_col])
            
            if sb_invert_y:
                self.ax[index_row,index_col].invert_yaxis()

            
                
            
            
                                                     

            
    def add_subplot_properties(self, index_row, index_col, xlim=None, ylim=None, title=None,
                               xlabel=None, ylabel=None, tick_size=14, label_size=18, set_legend=False,
                               legend_loc=None, text_str=None, multipleTextBox=None, text_fontsize=18, text_valign='top',
                               text_x_loc=0.8, text_y_loc=0.9, grid=True, xscale=None, yscale=None):
        
        """
        In order to provide the ability to add subplot properties to each subplot independently,
        an add_subplot_properties function was included. When initializing a class instance, you specify the number of nrows and ncols. This then
        allows for direct addressing of a specific row and column of the subplot matrix and adding properties to plot.
        
        :param index_row: what row of initialized figure's subplots to address (for a one subplot figure, use 0)
        :type index_row: int
        :param index_col: what col of initialized figure's subplots to address (for a one subplot figure, use 0)
        :type index_col: int
        :param xlim: specify xlim either as a lower bound or both lower and upper bound for addressed subplot
        :type xlim: int or list
        :param ylim: specify ylim either as a lower bound or both lower and upper bound for addressed subplot
        :type ylim: int or list
        :param title: specify a sub-title for the addressed subplot
        :type title: str
        :param xlabel: label for x-axis for addressed subplot of figure
        :type xlabel: str
        :param ylabel: label for x-axis for addressed subplot of figure
        :type ylabel: str
        :param tick_size: Size of each tick of addressed subplot of figure
        :type tick_size: int
        :param label_size: fontsize for both x and y axis labels
        :type label_size: int
        :param legend_loc: Specifies an explicit location for the legend of a given subplot of figure
        :type legend_loc: str
        """
        
        if xlim != None:
            if type(xlim) == int:
                self.ax[index_row,index_col].set_xlim(xlim)
            elif len(xlim) == 2:
                if type(xlim) == list or type(xlim) == tuple:
                    self.ax[index_row,index_col].set_xlim(xlim[0],xlim[1])
                    
        if ylim != None:
            if type(ylim) == int:
                self.ax[index_row,index_col].set_ylim(ylim)
            elif len(ylim) == 2:
                if type(ylim) == list or type(ylim) == tuple:
                    self.ax[index_row,index_col].set_ylim(ylim[0],ylim[1])
        

        if xscale in ["linear", "log", "symlog", "logit"]:
            self.ax[index_row,index_col].set_xscale(xscale)
      
        if yscale in ["linear", "log", "symlog", "logit"]:
            self.ax[index_row,index_col].set_yscale(yscale)
       
        if type(xlabel) == str: 
            if type(label_size) == int:
                self.ax[index_row,index_col].set_xlabel(xlabel, fontname='DejaVu Sans', fontsize=label_size)
            else:
                self.ax[index_row,index_col].set_xlabel(xlabel, fontname='DejaVu Sans')

        if type(ylabel) == str: 
            if type(label_size) == int:
                self.ax[index_row,index_col].set_ylabel(ylabel, fontname='DejaVu Sans', fontsize=label_size)
            else:
                self.ax[index_row,index_col].set_ylabel(ylabel, fontname='DejaVu Sans')
            
        if type(tick_size) == int:
            self.ax[index_row,index_col].tick_params(axis='x', labelsize=tick_size)
            self.ax[index_row,index_col].tick_params(axis='y', labelsize=tick_size)
        
        if type(title) == str:
            self.ax[index_row,index_col].set_title(title, fontname='DejaVu Sans', fontsize= label_size + 4)
   
        self.ax[index_row,index_col].grid(grid)
        
        if set_legend:
            if type(legend_loc) == str:
                self.ax[index_row,index_col].legend(loc=legend_loc, fontsize= label_size - 4)
            elif legend_loc == None:
                self.ax[index_row,index_col].legend(fontsize= label_size - 4)
            else:
                raise AssertionError
            
        if type(text_str) == str:
            props = dict(boxstyle='round', facecolor='white', alpha=0.5)
            self.ax[index_row,index_col].text(x=text_x_loc, y=text_y_loc, s=text_str, fontsize=text_fontsize, 
                                              verticalalignment=text_valign, transform=self.ax[index_row,index_col].transAxes, bbox=props)
        
        if type(multipleTextBox) == str: 
            props = dict(boxstyle='round', facecolor='white', alpha=0.5)
            self.ax[index_row,index_col].text(x=text_x_loc, y=text_y_loc, s=multipleTextBox, fontsize=text_fontsize, 
                                              transform=self.ax[index_row,index_col].transAxes, bbox=props)
            
       
    def export(self, filename=None, tight=True, save=False, reset=True, close=False):
        
        """
        There are two options here for exporting of the generated figure: plt.show() or plt.savefig(). In the case of the latter, 
        filename must be specified and save must be set to True. It is also ideal to set close to True so as to close the figure 
        after its been exported.
        
        :param filename: specify full directory path and filename to save figure to
        :type filename: str
        :param tight: sets plt.tight_layout boolean value
        :type tight: bool
        :param save: needs to be set to True in order to save figure to file
        :type save: bool
        :param reset: after exporting figure, will clear the figure but not close it
        :type reset: bool
        :param close: after exporting figure, will clear and close the figure
        :type close: bool
        """
        
        if tight:
            plt.tight_layout()
        
        if save and type(filename) == str:
            plt.savefig(filename)
        else:
            plt.show()
        
        if reset:
            plt.clf()
        
        if close:
            plt.close()
